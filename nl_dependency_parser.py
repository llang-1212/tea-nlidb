import stanza

nlp = stanza.Pipeline(lang='en', processors='tokenize,pos,constituency', package='default_accurate')
doc = nlp('Which cities have higher temperature in Feb than in Jun or have once served as host cities?')
for sentence in doc.sentences:
    print(sentence.constituency)


tree = doc.sentences[0].constituency
print(tree.label)
print(len(tree.children))
print(len(tree.children[0].children))
print(len(tree.children[0].children[0].children))
print(len(tree.children[0].children[0].children[0].children))
print()
print(tree.children[0].children[0].children[0].children[0].children)
