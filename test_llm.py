import json
import pprint

from fire import Fire
import pandas as pd
from chatgpt import get_database_schema, get_database_description, get_evidence_set, get_operation_annotations
import tqdm
from concurrent.futures import ThreadPoolExecutor, as_completed
import psycopg2


def get_db_conn():
    conn = psycopg2.connect("dbname='{db}' user='{user}' host='{host}' port='{port}' password='{passwd}'".format(
        user='postgres',
        passwd='ilovetony',
        host="localhost",
        port=5432,
        db="postgres",
        options="-c search_path=8803,public"
    ))
    return conn


df = pd.read_csv('./Spider Dataset Weak Labels.csv')


def generate_gpt_database_descriptions(db_id):
    database_schema = get_database_schema(db_id)
    # print(database_schema)
    database_description, message_context = get_database_description(database_schema)
    conn = get_db_conn()

    with conn.cursor() as cur:
        cur.execute(f"""INSERT INTO gatech."spider_dataset_GPT_database_description"(db_id, gpt_output, message_context) 
        VALUES (%s, %s,%s);""", (db_id, database_description, json.dumps(message_context)))
        conn.commit()
        cur.close()
    # print(database_description)


def generate_gpt_evidence_set(db_id):
    pbar = df[df['Column1.db_id'] == db_id].iterrows()
    conn = get_db_conn()
    with conn.cursor() as cur:
        cur.execute(
            f"SELECT db_id, message_context from gatech.\"spider_dataset_GPT_database_description\" where db_id = %s;",
            (db_id,))
        database_description = cur.fetchone()
        message_context = json.loads(database_description[1])

    for i, r in pbar:
        # pbar.set_description(f"{db_id} - {i}")
        question = r['Column1.question']
        evidence_set = get_evidence_set(message_context, question)
        with conn.cursor() as cur:
            cur.execute(f"""INSERT INTO gatech."spider_dataset_GPT_labels"(db_id, question, gpt_output)
                     VALUES (%s, %s,%s);""", (db_id, question, evidence_set,))
            conn.commit()
            cur.close()

    conn.close()


def generate_gpt_operation_annotations(db_id):
    pbar = df[df['Column1.db_id'] == db_id].iterrows()
    conn = get_db_conn()
    with conn.cursor() as cur:
        cur.execute(
            f"SELECT db_id, message_context from gatech.\"spider_dataset_GPT_database_description\" where db_id = %s;",
            (db_id,))
        database_description = cur.fetchone()
        message_context = json.loads(database_description[1])

    for i, r in pbar:
        # pbar.set_description(f"{db_id} - {i}")
        question = r['Column1.question']
        operation_annotations = get_operation_annotations(message_context, question)
        with conn.cursor() as cur:
            cur.execute(f"""INSERT INTO gatech."spider_dataset_GPT_operation_annotation"(db_id, question, gpt_output)
                     VALUES (%s, %s,%s);""", (db_id, question, operation_annotations,))
            conn.commit()
            cur.close()

    conn.close()


def main(each_database_operation, persistence_db_table_name):
    global df

    if persistence_db_table_name != 'gatech."spider_dataset_GPT_database_description"':
        existing_db = pd.read_sql(f"select * from {persistence_db_table_name}", get_db_conn())

        new_df = pd.merge(df, existing_db, how="outer", indicator=True,
                          left_on=['Column1.db_id', 'Column1.question'], right_on=['db_id', 'question']) \
            .query('_merge=="left_only"').drop('_merge', axis=1)

        df = new_df
    unique_db_ids = list(df['Column1.db_id'].unique())
    if len(unique_db_ids) == 0:
        print("Operation Complete.")
        return
    with tqdm.tqdm(total=len(unique_db_ids)) as pbar:
        with ThreadPoolExecutor(max_workers=len(unique_db_ids)) as ex:
            futures = [ex.submit(each_database_operation, db_id) for db_id in unique_db_ids]
            for future in as_completed(futures):
                result = future.result()
                pbar.update(1)


if __name__ == '__main__':
    Fire({
        'describe_database': lambda: main(generate_gpt_database_descriptions,
                                          'gatech."spider_dataset_GPT_database_description"'),
        'evidence_set': lambda: main(generate_gpt_evidence_set,
                                     'gatech."spider_dataset_GPT_labels"'),
        'operation_annotation': lambda: main(generate_gpt_operation_annotations,
                                             'gatech."spider_dataset_GPT_operation_annotation"'),
    })

    # get_gpt_database_descriptions('farm')
    # generate_gpt_operation_annotations('farm')
