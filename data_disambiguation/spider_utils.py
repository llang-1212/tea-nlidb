def extract_col_id_from_col_unit(col_unit):
    return [col_unit[1]]


def extract_col_ids_from_val_unit(val_unit):
    result = []
    _, col_unit1, col_unit2 = val_unit
    result.extend(extract_col_id_from_col_unit(col_unit1))
    if col_unit2 is not None:
        result.extend(extract_col_id_from_col_unit(col_unit2))
    return result

def extract_col_ids_from_table_unit(table_unit):
    table_type, col_unit_or_sql = table_unit
    if isinstance(col_unit_or_sql, dict):
        raise NotImplementedError
    else:
        return extract_col_id_from_col_unit(col_unit_or_sql)


def extract_col_ids_from_cond_unit(cond_unit):
    _, _, val_unit, _, _ = cond_unit
    return extract_col_ids_from_val_unit(val_unit)


def extract_col_ids_from_condition(condition):
    result = []
    for item in condition:
        if not isinstance(item, str):
            result.extend(extract_col_ids_from_cond_unit(item))
    return result


def extract_col_ids_from_sql(sql):
    result = []
    if sql is None:
        return result

    for aggId_valUnit in sql['select'][1]:
        result.extend(extract_col_ids_from_val_unit(aggId_valUnit[1]))

    result.extend(extract_col_ids_from_condition(sql['from']['conds']))
    result.extend(extract_col_ids_from_condition(sql['where']))
    for col_unit in sql['groupBy']:
        result.extend(extract_col_id_from_col_unit(col_unit))
    if len(sql['orderBy']) > 0:
        for val_unit in sql['orderBy'][1]:
            result.extend(extract_col_ids_from_val_unit(val_unit))
    result.extend(extract_col_ids_from_sql(sql['intersect']))
    result.extend(extract_col_ids_from_sql(sql['except']))
    result.extend(extract_col_ids_from_sql(sql['union']))
    return result


