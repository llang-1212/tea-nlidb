import pprint
import sqlite3
import warnings

# Suppress FutureWarning messages
warnings.simplefilter(action='ignore', category=UserWarning)

import pandas as pd

from concurrent.futures import ThreadPoolExecutor, as_completed
import tqdm
import psycopg2
import json
import os

from spider_code import eval_difficulty
from identify_and_clarify_confusing_ent_attr import \
    retrieve_and_summarize_multiple_llm_thought_on_confusing_ent_or_attr_in_db, enhance_understanding_on_confusing_ent_attrs
from sqlite_utils import SqliteDatabaseInfo
from spider_utils import extract_col_ids_from_sql

spider_table_json_path = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'tables.json')
spider_train_json_path = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'train_spider.json')
spider_database_root_path = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')
database_name = 'spider'


def get_db_conn():
    conn = psycopg2.connect("dbname='{db}' user='{user}' host='{host}' port='{port}' password='{passwd}'".format(
        user='postgres',
        passwd='ilovetony',
        host="localhost",
        port=5432,
        db="postgres",
        options="-c search_path=8803,public"
    ))
    return conn


def retrieve_summary_of_unclear_ent_attrs(db_id):
    db_path = os.path.join(spider_database_root_path, db_id, db_id + ".sqlite")
    collection_counter = retrieve_and_summarize_multiple_llm_thought_on_confusing_ent_or_attr_in_db(db_path)
    conn = get_db_conn()
    with conn.cursor() as cur:
        cur.execute(f"""INSERT INTO nebular."database_confusing_ent_attr" (dataset_name, db_id, collection_counter)
        VALUES (%s, %s,%s);""", (database_name, db_id, json.dumps(collection_counter)))
        conn.commit()
        cur.close()


def loop_retrieve_summary_of_unclear_ent_attrs():
    df = pd.read_json(spider_train_json_path)
    persistence_db_table_name = 'nebular."database_confusing_ent_attr"'
    existing_db_content = pd.read_sql(f"select * from {persistence_db_table_name}", get_db_conn())

    new_df = pd.merge(df, existing_db_content, how="outer", indicator=True,
                      left_on=['db_id'], right_on=['db_id']) \
        .query('_merge=="left_only"').drop('_merge', axis=1)

    unique_db_ids = list(new_df['db_id'].unique())

    if len(unique_db_ids) == 0:
        print("Operation Complete.")
        return
    # for db_id in unique_db_ids:

        # retrieve_summary_of_unclear_ent_attrs(db_id)
    with tqdm.tqdm(total=len(unique_db_ids)) as pbar:
        with ThreadPoolExecutor(max_workers=16) as ex:
            futures = [ex.submit(retrieve_summary_of_unclear_ent_attrs, db_id) for db_id in unique_db_ids]
            for future in as_completed(futures):
                result = future.result()
                pbar.update(1)


def annotate_database_elements_used_in_spider():
    table_df = pd.read_json(spider_table_json_path)
    train_df = pd.read_json(spider_train_json_path)
    conn = get_db_conn()
    db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')

    for i, r in train_df.iterrows():
        db_id = r['db_id']
        question = r['question']
        query = r['query']
        table_metadata = table_df[table_df['db_id'] == db_id].iloc[0]

        # print(r)
        r_sql = r['sql']
        col_ids = extract_col_ids_from_sql(r_sql)
        print(col_ids)
        db_elements = []
        for col_id in col_ids:
            table_idx, col_name = table_metadata['column_names_original'][col_id]
            if table_idx >= 0:
                table_name = table_metadata['table_names_original'][table_idx]
                print(table_name, col_name)
                db_elements.append(f"{table_name}.{col_name}")

        db_path = os.path.join(db_dir, db_id, db_id + ".sqlite")
        try:
            difficulty = eval_difficulty(db_path, query)
        except:
            difficulty = None
        with conn.cursor() as cur:
            cur.execute(f"""INSERT INTO nebular."query_database_elements" (dataset_name, db_id, question,  query, 
            db_elements, difficulty)
            VALUES (%s, %s,%s, %s, %s, %s);""", (database_name, db_id, question, query, db_elements, difficulty))
            conn.commit()
            cur.close()


def sample_simulated_query_logs(db_id, db_element):
    query_logs = pd.read_sql(f"select query from nebular.query_database_elements "
                             f"where db_id = '{db_id}' and '{db_element}' = Any (db_elements) and difficulty = 'easy' "
                             f"limit 4",
                             get_db_conn())['query'].tolist()
    additional_query_logs_needed = max(4 - len(query_logs), 3)
    # print(additional_query_logs_needed)

    db_path = os.path.join(spider_database_root_path, db_id, db_id + ".sqlite")
    db_info = SqliteDatabaseInfo(db_path)
    # TODO: swap operations in sample SQLs for Spider
    if additional_query_logs_needed > 0:
        table_name = db_element.split(".")[0]
        column_name =db_element.split(".")[1]

        samples = db_info.get_db_entities_samples([table_name])
        samples = samples[table_name][column_name].tolist()

        for s in samples:
            if s is not None:
                try:
                        float(s)
                        comparison_value_string = str(s)
                except ValueError:
                    comparison_value_string = '"' + s + '"'
                query_logs.append(f"SELECT {db_element} FROM {table_name} WHERE  {db_element} =  {comparison_value_string}")
    return query_logs

def sample_learn_and_create_schema_documentation(r):
    majority_confused_columns = []

    db_id = r['db_id']
    db_path = os.path.join(spider_database_root_path, db_id, db_id + ".sqlite")
    db_info = SqliteDatabaseInfo(db_path)
    print(db_info)

    for cc in sorted((r['collection_counter']).items(), key=lambda cc: cc[1], reverse=True):
        database_element, freq_count = cc
        if freq_count >= 5:
            try:
                majority_confused_columns.append(db_info.lower_to_correct(database_element))
            except KeyError:
                pass
    print(r['db_id'], len(majority_confused_columns), majority_confused_columns)

    confusing_ent_attrs_w_query_logs = []
    for mcc in majority_confused_columns:
        sample_query_logs = sample_simulated_query_logs(db_id, mcc)
        print(mcc)
        print(sample_query_logs)
        confusing_ent_attrs_w_query_logs.append((mcc, sample_query_logs))
    gpt_output, gpt_output_documentation = enhance_understanding_on_confusing_ent_attrs(db_info,
                                                                                        confusing_ent_attrs_w_query_logs)
    print(gpt_output)
    conn = get_db_conn()
    with conn.cursor() as cur:
        cur.execute(f"""INSERT INTO nebular."database_confusing_ent_attr_query_log_analysis" (dataset_name, db_id, gpt_output)
                VALUES (%s, %s,%s);""", (database_name, db_id, gpt_output))
        conn.commit()

        cur.execute(f"""INSERT INTO nebular."database_documentation" (dataset_name, db_id, gpt_output)
                VALUES (%s, %s,%s);""", (database_name, db_id, gpt_output_documentation))
        conn.commit()
        cur.close()

if __name__ == '__main__':
    # loop_retrieve_summary_of_unclear_ent_attrs()
    # import os
    # db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')
    # print(db_dir)
    # # db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'train', 'train', 'train_databases', 'train_databases')
    # db_id = 'soccer_2'
    # # db_id = 'icfp_1'
    # # db_id = 'movielens'
    # db_path = os.path.join(db_dir, db_id, db_id + ".sqlite")
    # difficulty = eval_difficulty(db_path, "SELECT pPos FROM tryout GROUP BY pPos ORDER BY count(*) DESC LIMIT 1")
    # print(difficulty)
    # sample_query_logs = sample_simulated_query_logs('wine_1', 'wine.Name')
    # print(sample_query_logs)
    # annotate_database_elements_used_in_spider()
    # df = pd.read_json(spider_train_json_path)
    # print(df)
    # for i, r in df[df['db_id'] == 'activity_1'].iterrows():
    #     print(r['db_id'])
    #     print(r['query'])
    #     print(r['sql'])

    df = pd.read_sql(f"SELECT * from nebular.database_confusing_ent_attr", get_db_conn())
    with tqdm.tqdm(total=len(df.sort_values(['db_id'], axis=0))) as pbar:
        with ThreadPoolExecutor(max_workers=16) as ex:
            futures = [ex.submit(sample_learn_and_create_schema_documentation, r) for _, r in df.sort_values(['db_id'], axis=0).iloc[:].iterrows()]
            for future in as_completed(futures):
                result = future.result()
                pbar.update(1)

    #
    #
    # for i, r in df.sort_values(['db_id'], axis=0).iloc[29:].iterrows():
    #     majority_confused_columns = []
    #
    #     db_id = r['db_id']
    #     db_path = os.path.join(spider_database_root_path, db_id, db_id + ".sqlite")
    #     db_info = SqliteDatabaseInfo(db_path)
    #     print(db_info)
    #
    #     for cc in sorted((r['collection_counter']).items(), key=lambda cc: cc[1], reverse=True):
    #         database_element, freq_count = cc
    #         if freq_count >= 5:
    #             try:
    #                 majority_confused_columns.append(db_info.lower_to_correct(database_element))
    #             except KeyError:
    #                 pass
    #     print(r['db_id'], len(majority_confused_columns), majority_confused_columns)
    #
    #     confusing_ent_attrs_w_query_logs = []
    #     for mcc in majority_confused_columns:
    #
    #         sample_query_logs = sample_simulated_query_logs(db_id, mcc)
    #         confusing_ent_attrs_w_query_logs.append((mcc, sample_query_logs))
    #     gpt_output, gpt_output_documentation = enhance_understanding_on_confusing_ent_attrs(db_info,
    #                                                                                         confusing_ent_attrs_w_query_logs)
    #     print(gpt_output)
    #     conn = get_db_conn()
    #     with conn.cursor() as cur:
    #         cur.execute(f"""INSERT INTO nebular."database_confusing_ent_attr_query_log_analysis" (dataset_name, db_id, gpt_output)
    #         VALUES (%s, %s,%s);""", (database_name, db_id, gpt_output))
    #         conn.commit()
    #
    #         cur.execute(f"""INSERT INTO nebular."database_documentation" (dataset_name, db_id, gpt_output)
    #         VALUES (%s, %s,%s);""", (database_name, db_id, gpt_output_documentation))
    #         conn.commit()
    #         cur.close()
