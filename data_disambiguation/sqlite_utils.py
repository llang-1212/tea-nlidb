import sqlite3

import pandas as pd


class SqliteDatabaseInfo:
    def __init__(self, sqlite_db_path):
        self.sqlite_db_path = sqlite_db_path
        self.schema, self.fk_relationships = self.retrieve_schema_and_fk_relationships()

        lower_to_correct_dict = {}
        for (t, cols) in list(self.schema.items()):
            lower_to_correct_dict[t.lower()] = t
            for (c, _) in cols:
                entity_attribute = f"{t}.{c}"
                lower_to_correct_dict[entity_attribute.lower()] = entity_attribute
        self.lower_to_correct_dict = lower_to_correct_dict


    def _get_db_tables(self, cursor):
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        tables = [str(table[0]) for table in cursor.fetchall()]
        return tables

    def _get_table_columnss(self, cursor, table_name):
        cursor.execute("PRAGMA table_info({})".format(table_name))
        columns = [(str(col[1]), str(col[2])) for col in cursor.fetchall()]
        return columns

    def _get_foreign_key_relationship(self, cursor, table_name):
        table_fk_relationships = {}
        cursor.execute("SELECT * FROM pragma_foreign_key_list('{}')".format(table_name))
        for r in cursor.fetchall():
            entity_attribute_pair_1 = table_name + '.' + r[3]
            if r[4] is not None:
                entity_attribute_pair_2 = r[2] + '.' + r[4]
            else:
                entity_attribute_pair_2 = r[2] + '.' + r[3]
            table_fk_relationships.setdefault(entity_attribute_pair_1, []).append(entity_attribute_pair_2)
            # table_fk_relationships.setdefault(entity_attribute_pair_2, []).append(entity_attribute_pair_1)
        return table_fk_relationships

    def retrieve_schema_and_fk_relationships(self):
        conn = sqlite3.connect(self.sqlite_db_path)
        cursor = conn.cursor()
        tables = self._get_db_tables(cursor)
        schema = {}
        fk_relationships = {}
        for t in tables:
            t_columns = self._get_table_columnss(cursor, t)
            schema[t] = t_columns
            fk_relationships = fk_relationships | self._get_foreign_key_relationship(cursor, t)
        return schema, fk_relationships

    def __repr__(self):
        returned_string = "Schema:\n"
        for table_name in self.schema.keys():
            returned_string += table_name
            returned_string += " (" + ', '.join([f"{str(c[0])}: {str(c[1])}" for c in self.schema[table_name]]) + ")\n"

        if len(self.fk_relationships.keys()) > 0:
            returned_string += "\nFK:\n"
            for fk in self.fk_relationships.keys():
                for fk_counter in self.fk_relationships[fk]:
                    returned_string += f"{fk} on {fk_counter}\n"

        return returned_string

    def lower_to_correct(self, lower_ent_attr):
        return self.lower_to_correct_dict[lower_ent_attr]

    def get_db_entities_samples(self, entities, limit=5):
        conn = sqlite3.connect(self.sqlite_db_path)
        data_samples = {}
        for table in entities:
            table_df = pd.read_sql("SELECT * FROM {} LIMIT {}".format(table, limit), conn)
            data_samples[table] = table_df
        return data_samples

# if __name__ == '__main__':
#     import os
#     db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')
#     print(db_dir)
#     # db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'train', 'train', 'train_databases', 'train_databases')
#     db_id = 'soccer_2'
#     # db_id = 'icfp_1'
#     # db_id = 'movielens'
#     db_path = os.path.join(db_dir, db_id, db_id + ".sqlite")
#     db_info = SqliteDatabaseInfo(db_path)
#     print(db_info)