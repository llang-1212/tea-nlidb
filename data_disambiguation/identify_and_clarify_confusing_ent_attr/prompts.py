from sqlite_utils import SqliteDatabaseInfo

system_prompt = f"""You are an external software development agent GPT creating an extensive database documentation. You and your client faithfully follow these principles:
1. A database is to be viewed as a collection of entities that correspond to the actual objects in the application environment. 
2. The entities in a database are organized into classes that are meaningful collections of entities. 
3. The classes of a database are not in general independent, but rather are logically related by means of interclass connections. 
4. Database entities and classes have attributes that describe their characteristics and relate them to other database entities. An attribute value may be 
derived from other values in the database. 
5. There are several primitive ways of defining interclass connections and derived attributes, corresponding to the most common types of information redundancy appearing in database applications. These facilities integrate multiple ways of viewing the same basic information, and provide building blocks for describing complex attributes and interclass relationships. 
"""

# The listed principles are from
# Michael Hammer and Dennis Mc Leod. 1981. Database description with
# SDM: a semantic database model. ACM Trans. Database Syst. 6, 3
# (Sept. 1981), 351–386. https://doi.org/10.1145/319587.319588


def prompt_for_thought_on_confusing_ent_or_attr(sqlite_database_info: SqliteDatabaseInfo):
    prompt = f"""List the confusing entities or attributes that needs further clarification from the client. Be very selective.\n\n{sqlite_database_info.__repr__()}"""
    return prompt


# Prompts for Parsing GPT's thought to Markdown Table
parser_system_prompt = "You are a specialized GPT response parser GPT that helps users parse each point the other GPT made abouting confusing attributes and entities. You should respond in markdown table with only the columns requested by the user and separate rows for each attribute even if they are part of the same entity."

def parser_initial_prompt(other_gpt_message):
    prompt = f"## Here's the response from another GPT:\n\n{other_gpt_message}\n\n## Markdown Table:\n| Entity | Attribute |\n|-------|-----------|"
    return prompt


def simulate_assistant_response_for_confusing_entity_attributes(confusing_entity_attributes: []):
    points_made = []
    for i, ent_attr in enumerate(confusing_entity_attributes):
        try:
            entity, attr = ent_attr.split(".")
        except:# TODO: make exception more specific
            continue
        points_made.append(f"{i+1}. \"{attr}\" attribute in the {entity} class is not very clear. It would be helpful if the client could provide a list of possible values or provide examples how this attribute is used in practice.")
    return points_made


def hint_w_samples_of_query_logs_to_solidify_data_domain(confusing_entity_attributes_w_query_logs: [(str,  [])]):
    prompt = f"""Those are valid points. Providing you a sample of SQL query logs from existing production database for each point. Use the query logs to improve your understanding. Some attributes can still be ambiguous with the provided query log and that's okay. In that case, specify whether it is a stat, performance, code ID, etc.  \n\n"""
    query_log_prompts = []
    for cea in confusing_entity_attributes_w_query_logs:
        entity, attr = cea[0].split(".")

        query_log_prompts.append(f"\"{attr}\" attribute in the {entity} entity\n" + "- " + '\n- '.join(cea[1]))

    return prompt + '\n\n'.join(query_log_prompts)


ask_to_explicity_disambguate = "You're on point. Explain what each attribute represents and if it's related to another attribute. In addition, explain how it is used in the database along with example values, data coverage, and data granularity if possibe. Lastly, explain how it could be used in context of the entire database and application domain.\n\n"

ask_llm_to_document_its_new_understanding = "Using the same format and content from your query log analysis above, but with divisons betweem entities using headers, create an extensive documentation on the schema. For each attribute, explain what it represents and if it's related to other attributes. In addition, explain how it is used in the database along with example values, data coverage, and data granularity if possibe."



"""
Based on the provided SQL query logs, here is an improved understanding of each attribute and its usage in the database:

1. "Phone" attribute in the Faculty entity:
   - Represents the phone number of a faculty member.
   - It is used to contact or communicate with the faculty.
   - Example values: 2424, 3593, 3402, 3121.
   - Data coverage: The query logs show that there are multiple distinct phone numbers present in the database.
   - Data granularity: The attribute stores phone numbers as integers.

2. "Major" attribute in the Student entity:
   - Represents the major or field of study of a student.
   - It indicates the area of specialization for a student's academic pursuits.
   - Example values: 600 (possibly representing a specific major code).
   - Data coverage: The query logs show that there are students with the same major code (600) in the database.
   - Data granularity: The attribute stores major codes as integers.

3. "Advisor" attribute in the Student entity:
   - Represents the advisor or mentor assigned to a student.
   - It indicates the faculty member responsible for guiding and advising the student's academic progress.
   - Example values: 1121, 7712, 7792, 8423.
   - Data coverage: The query logs show that there are students with different advisors in the database.
   - Data granularity: The attribute stores advisor IDs as integers.

4. "city_code" attribute in the Student entity:
   - Represents the city code or location of a student's residence.
   - It indicates the city where the student resides.
   - Example values: "BAL" (possibly representing Baltimore), "HKG" (possibly representing Hong Kong), "WAS" (possibly representing Washington), "CHI" (possibly representing Chicago).
   - Data coverage: The query logs show that there are students from different cities in the database.
   - Data granularity: The attribute stores city codes as strings.

5. "Building" attribute in the Faculty entity:
   - Represents the building where a faculty member's office is located.
   - It indicates the physical location of a faculty member's workspace.
   - Example values: "NEB" (possibly representing a specific building code).
   - Data coverage: The query logs show that there are faculty members located in the same building (NEB) in the database.
   - Data granularity: The attribute stores building codes as strings.

6. "Rank" attribute in the Faculty entity:
   - Represents the rank or position of a faculty member.
   - It indicates the level of seniority or academic standing of a faculty member.
   - Example values: "Instructor", "Professor".
   - Data coverage: The query logs show that there are faculty members with different ranks in the database.
   - Data granularity: The attribute stores rank names as strings.

By analyzing the query logs and understanding the context of each attribute, we can now provide a clearer explanation of their meanings, relationships, and usage in the database.
"""