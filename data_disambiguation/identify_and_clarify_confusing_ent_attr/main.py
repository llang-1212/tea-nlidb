import collections
import sys

import openai
import pprint

import tqdm

import configs
from sqlite_utils import SqliteDatabaseInfo
import md_utils

from . import prompts


def retrieve_one_llm_thought_on_confusing_ent_or_attr_in_schema(sqlite_database_info: SqliteDatabaseInfo):
    messages = [
        {
            "role": "system",
            "content": prompts.system_prompt
        },
        {
            "role": "user",
            "content": prompts.prompt_for_thought_on_confusing_ent_or_attr(sqlite_database_info)
        },
    ]
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo-16k-0613",
        messages=messages,
        temperature=0.8,
        max_tokens=10000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0,
        api_key=configs.openai_api_key
    )
    gpt_output = response.choices[0].message.content
    return gpt_output


def parse_one_llm_thought_on_confusing_attributes_in_schema(gpt_output):
    messages = [
        {
            "role": "system",
            "content": prompts.parser_system_prompt
        },
        {
            "role": "user",
            "content": prompts.parser_initial_prompt(gpt_output)
        },
    ]
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo-16k-0613",
        messages=messages,
        temperature=0.2,
        max_tokens=10000,
        top_p=0.1,
        frequency_penalty=0,
        presence_penalty=0,
        stop="#",

        api_key=configs.openai_api_key
    )
    # pprint.pprint(response)
    parser_output = response.choices[0].message.content

    return md_utils.from_md(parser_output)


def retrieve_and_summarize_multiple_llm_thought_on_confusing_ent_or_attr_in_db(db_path):
    db_info = SqliteDatabaseInfo(db_path)
    confusing_columns = []
    for i in tqdm.tqdm(range(10)):
        one_llm_thought = retrieve_one_llm_thought_on_confusing_ent_or_attr_in_schema(db_info)
        one_llm_thought_md = parse_one_llm_thought_on_confusing_attributes_in_schema(one_llm_thought)
        print(one_llm_thought_md)
        confusing_columns += (one_llm_thought_md.iloc[:, 0].astype(str) + '.' + one_llm_thought_md.iloc[:, 1].astype(
            str)).tolist()
    return collections.Counter([str(i).lower() for i in confusing_columns])


def enhance_understanding_on_confusing_ent_attrs(sqlite_database_info: SqliteDatabaseInfo, confusing_ent_attrs_w_query_logs):
    simulated_assistant_response_for_confusing_ent_attrs = prompts. \
        simulate_assistant_response_for_confusing_entity_attributes(
        [cea[0] for cea in confusing_ent_attrs_w_query_logs]
    )
    messages = [
        {
            "role": "system",
            "content": prompts.system_prompt
        },
        {
            "role": "user",
            "content": prompts.prompt_for_thought_on_confusing_ent_or_attr(sqlite_database_info)
        },
        {
            "role": "assistant",
            "content": '\n\n'.join(simulated_assistant_response_for_confusing_ent_attrs)
        },
        {
            "role": "user",
            "content": prompts.hint_w_samples_of_query_logs_to_solidify_data_domain(confusing_ent_attrs_w_query_logs)
        }
    ]
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo-16k-0613",
        messages=messages,
        temperature=0.8,
        max_tokens=10000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0,
        api_key=configs.openai_api_key
    )
    gpt_output = response.choices[0].message.content

    pprint.pprint(gpt_output)

    messages.append(
        {
            "role": "assistant",
            "content": gpt_output
        }
    )
    messages.append(
        {
            "role": "user",
            "content": prompts.ask_to_explicity_disambguate
        }
    )
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo-16k-0613",
        messages=messages,
        temperature=0.8,
        max_tokens=10000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0,
        api_key=configs.openai_api_key
    )
    gpt_disambiguation = response.choices[0].message.content

    messages.append(
        {
            "role": "assistant",
            "content": gpt_disambiguation
        }
    )

    messages.append(
        {
            "role": "user",
            "content": prompts.ask_llm_to_document_its_new_understanding
        }
    )
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo-16k-0613",
        messages=messages,
        temperature=0.3,
        max_tokens=8000,
        top_p=0.2,
        frequency_penalty=0,
        presence_penalty=0,
        api_key=configs.openai_api_key
    )
    gpt_output_documentation = response.choices[0].message.content
    return gpt_output, gpt_output_documentation

    # confusing_ent_attr = retrieve_and_summarize_multiple_llm_thought_on_confusing_ent_or_attr_in_db(db_path)

