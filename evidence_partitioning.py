from fastcoref import FCoref

model = FCoref(device="cuda:0")


def heuristics_co_reference(new_es1, nqt, new_es2):
    # pretend we found co-referenced tokens and add them in ES2
    new_es1 = new_es1
    new_es2 = new_es2
    full_Q = new_es1 + [nqt] + new_es2
    full_Q_text = [e.characters for e in new_es1] + [nqt.characters] + [e.characters for e in new_es2]
    nqt_idx = nqt.word_idx

    preds = model.predict(
        texts=[full_Q_text],
        is_split_into_words=True
    )
    co_reference_clusters = preds[0].get_clusters(as_strings=False)

    if len(co_reference_clusters) == 0:  # no change required
        # print()
        return new_es1, new_es2
    else:
        for c in co_reference_clusters:
            word_before_nqt_present = False
            word_after_nqt_present = False
            for word_idx in c:
                if word_idx[0] < nqt_idx:
                    word_before_nqt_present = True
                if word_idx[0] > nqt_idx:
                    word_after_nqt_present = True
            if word_before_nqt_present and word_after_nqt_present:
                # get evidence set in es1

                for word_idx in c:
                    if full_Q[word_idx[0]].related_database_column is not None:
                        new_es2.append(full_Q[word_idx[0]])
                    elif full_Q[word_idx[0]].related_database_table is not None:
                        new_es2.append(full_Q[word_idx[0]])
        return new_es1, new_es2

def heuristics_2(es1, nqt, es2):
    # pretend we found co-referenced tokens and add them in ES2
    full_Q = [e.characters for e in es1] + [nqt.characters] + [e.characters for e in es2]

    preds = model.predict(texts=[full_Q], is_split_into_words=True)
    if len(preds[0].get_clusters()) == 0:  # no change required
        return es1, es2


def heuristics_instance_sharing(es1, nqt, es2):
    # pretend we found co-referenced tokens and add them in ES2
    for e in es1:
        if e.is_instance_type:
            if e.related_database_table is not None:
                es2.append(e)

    for e in es2:
        if e.is_instance_type:
            if e.related_database_table is not None:
                es1.append(e)

    return es1, es2

def heuristics_focus_sharing(es1, nqt, es2):
    for e in es1:
        if e.focus:
            es2.append(e)
    return es1, es2

# def heuristics_argument_sharing(es1, nqt, es2):
#     for ee in es1:
#         if e.related_database_table is not None:
#             if e.dependent_idx > nqt.word_idx:
#                 pass


def heuristics_instance_sharing_in_numeric_comparison(es1, nqt, es2):
    for e in es1:
        if e.operation_annotation == 'comparison' or e.operation_annotation == 'equality':
            if e.is_instance_type:
                es2.append(e)
    return es1, es2

def apply_heuristics(es1_before, nqt, es2_after, *heuristics):
    new_es1_before = set(es1_before)
    new_es2_after = set(es2_after)
    for h in heuristics:
        es1_returned, es2_returned = h(es1_before, nqt, es2_after)
        new_es1_before.update(es1_returned)
        new_es2_after.update(es2_returned)
    return new_es1_before, new_es2_after
# if __name__ == '__main__':
#     heuristics_1(None, None)
#


# if __name__ == '__main__':
#     import spacy
#     nlp = spacy.load("en_core_web_trf")
#     spacy_tokens = nlp("Show me everyone who bought stocks in 2019 that have gone up in value.")
#     for st in spacy_tokens:
#         print(st, st.dep_)
#         if st.dep_ == 'ROOT':
#             for c in st.children:
#                 print(c.idx, c)
#             print(st.children)