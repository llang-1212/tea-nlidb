import string

import numpy as np
import pandas as pd
import psycopg2
# database_metadata_df = pd.read_json("tables.json")

from difflib import SequenceMatcher as SM
from nltk.util import ngrams


def is_close_subsequence(main_str, substr, max_skips=2):
    main_str = main_str.translate(str.maketrans('', '', string.punctuation))
    main_words = main_str.split()
    sub_words = substr.split()

    def match_from_index(start_i, sub_words):
        skips = 0
        j = 0  # Index for sub_words
        for i in range(start_i, len(main_words)):
            if main_words[i] == sub_words[j]:
                j += 1
                if j == len(sub_words):
                    # Reached the end of sub_words, all words matched
                    return i, skips
            else:
                skips += 1
                if skips > max_skips:
                    # Too many skips, not a match from this start index
                    return -1, skips
        return -1, skips

    closest_match = float('inf')  # Initialize closest match distance to infinity
    for i, word in enumerate(main_words):
        if word == sub_words[0]:  # When the first word of the subsequence is found
            end_idx, num_skips = match_from_index(i, sub_words)
            if end_idx != -1:
                # Calculate the 'distance' to this subsequence
                current_distance = i + len(sub_words) - 1
                return i, end_idx + 1
                closest_match = min(closest_match, num_skips)

    # If we found a closest match, return True; otherwise, False
    return -1, -1


def finding_substring_idx(needle, hay):

    # needle = "this is the string we want to find"
    # hay = "text text lots of text and more and more this string is the one we wanted to find and here is some more and even more still"

    needle_length = len(needle.split())
    max_sim_val = 0
    max_sim_string = u""

    for ngram in ngrams(hay.split(), needle_length + int(.2 * needle_length)):
        hay_ngram = u" ".join(ngram)
        similarity = SM(None, hay_ngram, needle).ratio()
        if similarity > max_sim_val:
            max_sim_val = similarity
            max_sim_string = hay_ngram
    index = hay.find(max_sim_string)
    return index, index + len(max_sim_string)



# def get_database_tables_metadata(db_id):
#     db_metadata = database_metadata_df[database_metadata_df['db_id'] == db_id]
#     # print(db_metadata)
#     columns_dict = {}
#     columns_type = list(db_metadata['column_types'])[0]
#
#     for column, column_type in zip(list(db_metadata['column_names_original'])[0], columns_type):
#         table_idx, column_name = column
#         if table_idx == -1:
#             continue
#         if table_idx not in columns_dict.keys():
#             columns_dict[table_idx] = []
#         columns_dict[table_idx].append(column_name + ' ' + column_type)
#
#     return columns_dict


def get_db_conn():
    conn = psycopg2.connect("dbname='{db}' user='{user}' host='{host}' port='{port}' password='{passwd}'".format(
        user='postgres',
        passwd='',

        host="localhost",
        port=5432,
        db="postgres",
        options="-c search_path=8803,public"
    ))
    return conn


import re

import pandas as pd


def _is_header(extracted):
    """
    >>> _is_header(["---", "---"])
    True
    >>> _is_header([":---", "---:", ":---:"])
    True
    >>> _is_header([":---", "foo", "bar"])
    False
    >>> _is_header(["foo", "bar"])
    False
    """
    partial_pattern = r":*-+:*"
    return all(re.match(partial_pattern, ex) for ex in extracted)


def _extract_line(line: str, possible_separator):
    """
    >>> _extract_line("| foo | bar |", False)
    (['foo', 'bar'], False)
    >>> _extract_line("| foo | bar |", True)
    (['foo', 'bar'], False)
    >>> _extract_line("| --- | --- |", False)
    (['---', '---'], False)
    >>> _extract_line("| --- | --- |", True)
    ([], True)
    """
    # need to accept overlapping patterns.
    vertical_pattern = r"(?=(\|(.*?)\|))"
    plus_pattern = r"(?=(\+(.*?)\+))"
    extracted = [value.strip() for _, value in re.findall(vertical_pattern, line)]
    if possible_separator and not extracted:
        # check this pattern's separator, ex. +-----+----+
        extracted = [value.strip() for _, value in re.findall(plus_pattern, line)]

    if not extracted:
        return [], False

    if possible_separator and _is_header(extracted):
        return [], True

    return extracted, False


def from_md(table: str, header=None):
    """

    Args:
        table (str): a markdown table
        header (list, optional): a header of the columns

    Returns:
        pd.DataFrame
    """
    rows = []
    for line in table.split("\n"):
        extracted, is_header = _extract_line(line.strip(), len(rows) == 1)
        if is_header:
            if header is None:
                header = rows[0]
            rows.pop(0)
            continue

        if extracted == []:
            continue

        rows.append(extracted)

    return pd.DataFrame(rows, columns=header)


# def num_tokens_from_messages(messages, model="gpt-3.5-turbo-0613"):
#   """Returns the number of tokens used by a list of messages."""
#   try:
#       encoding = tiktoken.encoding_for_model(model)
#   except KeyError:
#       encoding = tiktoken.get_encoding("cl100k_base")
#   if model == "gpt-3.5-turbo-0613":  # note: future models may deviate from this
#       num_tokens = 0
#       for message in messages:
#           num_tokens += 4  # every message follows <im_start>{role/name}\n{content}<im_end>\n
#           for key, value in message.items():
#               num_tokens += len(encoding.encode(value))
#               if key == "name":  # if there's a name, the role is omitted
#                   num_tokens += -1  # role is always required and always 1 token
#       num_tokens += 2  # every reply is primed with <im_start>assistant
#       return num_tokens
#   else:
#       raise NotImplementedError(f"""num_tokens_from_messages() is not presently implemented for model {model}.
#   See https://github.com/openai/openai-python/blob/main/chatml.md for information on how messages are converted to tokens.""")