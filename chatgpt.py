import time

import pandas as pd
import openai
import os




database_metadata_df = pd.read_json("tables.json")


def get_database_schema(db_id, return_table_dict=False, return_table_dict_w_table_name=False):
    db_metadata = database_metadata_df[database_metadata_df['db_id'] == db_id]
    # print(db_metadata)
    columns_dict = {}
    columns_dict_w_table_names = {}
    columns_type = list(db_metadata['column_types'])[0]
    table_names = list(db_metadata['table_names_original'])[0]
    for column, column_type in zip(list(db_metadata['column_names_original'])[0], columns_type):
        table_idx, column_name = column
        if table_idx == -1:
            continue
        if table_idx not in columns_dict.keys():
            columns_dict[table_idx] = []
            columns_dict_w_table_names[table_names[table_idx].lower()] = {}
        columns_dict[table_idx].append(column_name + ' ' + column_type)
        columns_dict_w_table_names[table_names[table_idx].lower()][column_name.lower()] = column_type

    if return_table_dict:
        return columns_dict

    if return_table_dict_w_table_name:
        return columns_dict_w_table_names

    schema_string = ""
    for i, table in enumerate(list(db_metadata['table_names_original'])[0]):
        schema_string += table
        schema_string += " (" + ', '.join(columns_dict[i]) + ")"
        if i < len(list(db_metadata['table_names_original'])[0]) - 1:
            schema_string += '\n'

    return schema_string


def get_database_description(database_schema):
    message_context = [
            {"role": "system", "content": "You are ChatGPT, a large language model trained by OpenAI, based on the GPT-3.5 architecture. Knowledge cutoff: 2021-09 Current date: 2023-10-15"},
            {
            "role": "user", "content": f"Write a description what each table in the given schema contains.\n\
Schema:\n\
SUPPLIER SUPPLIER (SNO number, SNAME text, SLOC text, SBUDGET number)\n\
PART (PNO number, PNAME text, DIM number, PRICE number, COLOR text) \n\
PROJECT (JNO number, JNAME text, PNO number, JBUDGET number, JLOC text) \n\
SHIPMENT (SNO number, PNO number, JNO number, QTY number) \n\
\n\
Description:\n\
Each SUPPLIER tuple contains the number (identifier), name, location, and  budget of a supplier. Each PART tuple contains the number (identifier), name, dimension, unit price, and color of a part. A PROJECT tuple has fields for the  number (identifier), name, budget, and location of a project, along with the part  number of a part it uses. A SHIPMENT tuple has fields for a supplier number,  along with the part number, project number, and quantity of the part the supplier supplies.\n\
\n\
Schema:\n\
{database_schema}\n\
Description:"}]

    completed = False
    while not completed:
        try:
            chat_completion = openai.ChatCompletion.create(
                model="gpt-3.5-turbo-0613",
                temperature=0,
                messages=message_context)
            completed = True
        except openai.error.RateLimitError:
            print("get_database_description - openai.error.RateLimitError")
            time.sleep(120)

    message_context += [{"role": "assistant", "content": chat_completion.choices[0].message.content}]
    return chat_completion.choices[0].message.content, message_context


def get_evidence_set(message_context, natural_language_query):
    # natural_language_query = "Show the status of the town that has hosted the greatest number of contests."
    prompt = f"Associate a token t with one or more ontology elements including concepts, relationships,\
and data properties. Also annotate tokens that indicate time ranges and then associates them with the ontology properties \
whose corresponding data type is time-related (e.g., Date). Finally, annotate tokens that mention numeric quantities, either \
in the form of numbers or in text, and subsequently match them to ontology properties with numerical data types (e.g., Double). \
After you finish annotating, create a table that maps token to ontology elements.\
\n\
Query:\n\
Find the supplier numbers of suppliers which supply parts whose part number is P1.\n\
\n\
Table:\n\
| Token            | Ontology Element |\n\
| ---------------- | ---------------- |\n\
| supplier numbers | `SHIPMENT.SNO`   |\n\
| part numbers     | `SHIPMENT.PNO`   |\n\
\n\
Query:\n\
Find the names of suppliers which supply all red parts that are priced higher than 25.\n\
\n\
Table:\n\
| Token                                | Ontology Element |\n\
| ------------------------------------ | ---------------- |\n\
| names of suppliers                   | `SUPPLIER.SNAME` |\n\
| red parts                            | `PART.COLOR`     |\n\
| parts that are priced higher than 25 | `PART.PRICE`     |\n\
\n\
Query:\n\
{natural_language_query}\n\
\n\
Table:"
    # print(prompt)
    completed = False
    while not completed:
        try:

            chat_completion = openai.ChatCompletion.create(
                temperature=0,
                model="gpt-3.5-turbo-0613",
                messages=message_context + [{"role": "user", "content": prompt}]
            )

            completed = True
        except openai.error.RateLimitError:
            print("get_evidence_set - openai.error.RateLimitError")

            time.sleep(120)

    return chat_completion.choices[0].message.content


def get_operation_annotations(message_context, natural_language_query):
    prompt = f"Annotate words in the natural language query for linguistic patterns to count (e.g., count of, number of, how many, etc.), aggregation (e.g., total, sum, max, min, average, etc.), comparison (e.g., more then, less than, etc., equal, same, also, too, etc., not equal, different, another, etc.), or negation (e.g., no ,not, none, nothing, etc.). Annotate words ONLY IF THEY ARE APPLICABLE. \
    After you finish annotating, create a table that maps words to annotations along with reasoning based on database operations. Do not create additional examples of pairs of natural language query and annotation. Answer only the last given natural language query.\
    \n\
    Query:\n\
    Find the highest part number of parts whose unit price is greater than 25.\n\
    \n\
    Table:\n\
    | Tokens                        | Linguistic Pattern   | Reasoning        |\n\
    | ----------------------------- | -------------------- | ---------------- |\n\
    | highest part number           | aggregation          |  MAX(PART.PNO)   |\n\
    | unit price is greater than 25 | comparison           |  PART.PRICE > 25 |\n\
    \n\
    Query:\n\
    Find the names of suppliers that do not supply parts whose part number is P1.\n\
    \n\
    Table:\n\
    | Tokens                        | Linguistic Pattern   | Reasoning                 |\n\
    | ----------------------------- | -------------------- | ------------------------- |\n\
    | do not supply                 | negation             |  SUPPLER.SNO IS NOT IN    |\n\
    | parts whose part number is P1 | comparison           |  SHIPMENT.PNO = ‘P1’      |\n\
    \n\
    Query:\n\
    {natural_language_query}\n\
    \n\
    Table:"

    completed = False
    while not completed:
        try:

            chat_completion = openai.ChatCompletion.create(
                temperature=0,
                model="gpt-3.5-turbo-0613",
                messages=message_context + [{"role": "user", "content": prompt}]
            )

            completed = True
        except openai.error.RateLimitError:
            print("get_evidence_set - openai.error.RateLimitError")

            time.sleep(120)

    return chat_completion.choices[0].message.content


if __name__ == '__main__':
    database_schema = get_database_schema('soccer_2')
    # database_description, message_context = get_database_description(database_schema)
    # operation_annotation = get_operation_annotations(message_context,
    #                                            "How many books are there?")
    print(database_schema)
    # with open('chatgpt_response.txt', 'w') as f:
    #     f.write(database_description)
    #
    # evidence_set = get_evidence_set(message_context)
    # # evidence_set = get_evidence_set(database_description)
    # #
    # with open('chatgpt_evidence_set_response.txt', 'a+') as f:
    #     f.write(evidence_set)