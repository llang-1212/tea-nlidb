from typing import List

import spacy

from utils import is_close_subsequence
from chatgpt import get_database_schema


class AnnotatedToken:
    def __init__(self, i, characters):
        self.characters = characters
        self.related_database_table = None
        self.related_database_column = None
        self.operation_annotation = None
        self.raw_operation_annotation = None
        self.is_measure = False
        self.word_idx = i
        self.is_instance_type = False

        # Spacy Annotations
        self.numeric = False
        self.dependents_idxs = []
        self.part_of_speech = None
        self.focus = False
        # nested query token annotation
        self.nested_query_type = None


    def set_associated_database_element(self, table_name, column_name):
        self.related_database_table = table_name
        self.related_database_column = column_name

    def set_operation_annotation(self, operation_type, raw_operation_annotation):
        self.operation_annotation = operation_type
        self.raw_operation_annotation = raw_operation_annotation

    def set_measure_annotation(self, is_measure: bool):
        self.is_measure = is_measure

    def set_numeric_annotation(self, numeric_annotation: bool):
        self.numeric = numeric_annotation

    def set_dependent_idxs(self, dependent_idxs):
        self.dependents_idxs = dependent_idxs

    def set_nested_query_type(self, nested_query_type_annotation):
        self.nested_query_type = nested_query_type_annotation

    def set_part_of_speech_category(self, part_of_speech_category):
        self.part_of_speech = part_of_speech_category

    def set_focus(self, is_focus):
        self.focus = is_focus

    def set_instance_type(self, is_instance_type):
        self.is_instance_type = is_instance_type

    def __str__(self):
        return f"""{self.characters} | {self.related_database_table} | {self.related_database_column} {self.operation_annotation} | {self.raw_operation_annotation} | {self.is_measure} | {self.word_idx} | # Spacy Annotations {self.numeric} {self.dependents_idxs} | # nq annot: {self.nested_query_type}"""


    def __eq__(self, other):
        return self.characters == other.characters and self.word_idx == other.word_idx

    def __hash__(self):
        return hash((self.characters, self.word_idx))


def remove_punctuation(input_string):
    # punctuation marks
    punctuations = '''!()[]{};:'"\,<>./?@#$%^&*_~'''

    # traverse the given string and if any punctuation
    # marks occur replace it with null
    for x in input_string.lower():
        if x in punctuations:
            input_string = input_string.replace(x, "")

    # Print string without punctuation
    return input_string


def tokenize(Q):
    return [sub for sub in Q.split()]


def convert_string_idx_to_token_idx(sentence, string_idx):
    tokenized_query = tokenize(sentence[:string_idx])
    return len(tokenized_query)


def create_annotated_tokens(database_tables_info,
                            Q,
                            gpt_evidence_set_postprocessed,
                            gpt_operation_annotation_postprocessed,
                            ):
    nlp = spacy.load("en_core_web_sm")
    spacy_parsed = nlp(Q)

    tokenized_query = tokenize(Q)
    token_annotations = [AnnotatedToken(i, t) for i, t in enumerate(tokenized_query)]
    for es in gpt_evidence_set_postprocessed:
        tokens, table_name, column_name = es['tokens'], es['table_name'], es['column_name']

        is_measure = False
        if table_name is not None and table_name.lower() in database_tables_info:
            if column_name is not None and column_name.lower() in database_tables_info[table_name.lower()]:
                column_datatype = database_tables_info[table_name.lower()][column_name.lower()]
                is_measure = column_datatype == 'number'
        start_idx, end_idx = is_close_subsequence(Q, tokens)
        for token_annot in token_annotations[start_idx:end_idx]:
            token_annot.set_associated_database_element(table_name, column_name)
            token_annot.set_measure_annotation(is_measure)
    for annot in gpt_operation_annotation_postprocessed:
        tokens, operation_type, annotation = annot['tokens'], annot['operation_type'], annot['annotation']
        if len(tokens) == 0:
            continue
        # print(tokens, operation_type, annotation)
        start_idx, end_idx = is_close_subsequence(Q, tokens)
        # print(start_idx, end_idx)
        for token_annot in token_annotations[start_idx:end_idx]:
            token_annot.set_operation_annotation(operation_type, annotation)

    for t_annot, spacy_token in zip(token_annotations, spacy_parsed):
        t_annot.set_numeric_annotation(spacy_token.like_num)
        t_annot.set_dependent_idxs(
            [convert_string_idx_to_token_idx(Q, child.idx) for child in spacy_token.children] +
            [convert_string_idx_to_token_idx(Q, spacy_token.head.idx)]
        )

    return token_annotations


def detect_nested_query_type(db_id,
                             Q,
                             gpt_evidence_set_postprocessed,
                             gpt_operation_annotation_postprocessed):
    database_tables_info = get_database_schema(db_id, return_table_dict=True)
    token_annotations = create_annotated_tokens(database_tables_info,
                                                Q,
                                                gpt_evidence_set_postprocessed,
                                                gpt_operation_annotation_postprocessed
                                                )
    nqTokens = []
    aggTokens = []
    joinTokens = []

    for t_annot in token_annotations:
        if t_annot.operation_annotation == 'COUNT' or t_annot.operation_annotation == 'AGGREGATION':
            aggTokens.append(t_annot)
        elif t_annot.operation_annotation == 'COMPARISON' or t_annot.operation_annotation == 'NEGATION':
            joinTokens.append(t_annot)
    # print(token_annotations)
    # print(aggTokens)
    # print(joinTokens)
    if not aggTokens and joinTokens:
        for jt in joinTokens:
            deps = jt.dependents_idxs
            dts_entity = [token_annotations[d] for d in deps if d < len(token_annotations) and token_annotations[d].related_database_table is not None ]

            try:
                jt_prev = token_annotations[jt.word_idx - 1]
            except IndexError:
                jt_prev = None
            try:
                jt_next = token_annotations[jt.word_idx + 1]
            except IndexError:
                jt_next = None

            # print(dts_entity)
            # print(jt_prev)
            # print(jt_next)

            if jt.operation_annotation == 'NEGATION' or (
                    jt.operation_annotation == 'COMPARISON' and '=' in jt.raw_operation_annotation):
                if jt_next is not None and jt_next in dts_entity and \
                        jt_prev is not None and jt_prev in dts_entity and \
                        jt_next.related_database_table == jt_prev.related_database_table:
                    jt.nested_query_type = 'TYPE-N'
                elif (jt_next is not None and jt_next in dts_entity) or (jt_prev is not None and jt_prev in dts_entity):
                    jt.nested_query_type = 'TYPE-J'

            if jt.operation_annotation == 'COMPARISON' and \
                    jt_prev is not None and jt_prev.is_measure and \
                    jt_next is not None and jt_next.numeric:
                jt.nested_query_type = 'TYPE-N'
            nqTokens.append(jt)

    elif aggTokens and not joinTokens:
        for at in aggTokens:
            if at.operation_annotation == 'AGGREGATION':
                deps = at.dependents_idxs
                for d in deps:
                    if d >= len(token_annotations):
                        continue
                    dt = token_annotations[d]
                    if dt.related_database_table is not None:
                        if at.word_idx > dt.word_idx:
                            at.nested_query_type = 'TYPE-A'
                            nqTokens.append(at)
                            break

    elif aggTokens and joinTokens:
        for jt in joinTokens:
            deps = jt.dependents_idxs
            dts_measure = [token_annotations[d] for d in deps if (d < len(token_annotations) and token_annotations[d].is_measure)]
            try:
                jt_prev = token_annotations[jt.word_idx - 1]
            except IndexError:
                jt_prev = None
            if jt.operation_annotation == 'COMPARISON' and jt_prev in dts_measure:
                jt.nested_query_type = 'TYPE-JA'
                nqTokens.append(jt)

        for at in aggTokens:
            deps = at.dependents_idxs
            dts_entity = [token_annotations[d] for d in deps if (d < len(token_annotations) and token_annotations[d].related_database_table is not None)]
            dts_comparison = [token_annotations[d] for d in deps if (d < len(token_annotations) and token_annotations[d].operation_annotation == 'COMPARISON')]
            if at.operation_annotation == 'AGGREGATION' and True and len(dts_entity) > 0 and len(dts_comparison) > 0:
                at.nested_query_type = 'TYPE-JA'
                nqTokens.append(at)

    return nqTokens


def evidence_partitioning_heuristics_1():
    pass

def evidence_partitioning_heuristics_2():
    pass

def evidence_partitioning_heuristics_3():
    pass

def evidence_partitioning_heuristics_4():
    pass

def evidence_partitioning_heuristics_5():
    pass

def evidence_partitioning_heuristics_6():
    pass


def evidence_partitioning_algorithm(nqTokens, annotated_tokens):


    ListES = []
    for nqt in nqTokens:
        ES1 = ES.before(nqTokens)
        ES2 = ES.after(nqTokens)

        if nqt.nested_query_type == 'TYPE-N':
            pass
        elif nqt.nested_query_type == 'TYPE-A':
            pass
        elif nqt.nested_query_type == 'TYPE-J':
            pass
        elif nqt.nested_query_type == 'TYPE-JA':
            pass
        ListES.append(ES1)
        ES = ES2
    ListES.append(ES)
    return ListES
