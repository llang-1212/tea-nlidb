import difflib
import os
import re
import sqlite3
import time
from concurrent.futures import ThreadPoolExecutor, as_completed

import numpy as np
import openai
import pandas as pd
import psycopg2
import tqdm
from fire import Fire
import sqlparse
import sqlparse.sql
import sqlglot

from join_path_generator import join_path_generator, get_lower_to_correct_dict
from rapidfuzz import process, fuzz


df = pd.read_csv('spider_gpt_keyword_operation-annotations.csv')


def get_db_conn():
    conn = psycopg2.connect("dbname='{db}' user='{user}' host='{host}' port='{port}' password='{passwd}'".format(
        user='postgres',
        passwd='ilovetony',

        host="localhost",
        port=5432,
        db="postgres",
        options="-c search_path=8803,public"
    ))
    return conn




def get_db_tables(db_loc):
    conn = sqlite3.connect(db_loc)
    cursor = conn.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = [str(table[0]) for table in cursor.fetchall()]
    return tables


def get_db_schema(db_loc, entities):
    conn = sqlite3.connect(db_loc)
    cursor = conn.cursor()
    schema = {}

    # fetch table names
    for table in entities:
        cursor.execute("PRAGMA table_info({})".format(table))
        schema[table] = [str(col[1]) for col in cursor.fetchall()]

    return schema


def get_db_entities_samples(db_loc, entities, limit=5):
    conn = sqlite3.connect(db_loc)
    cursor = conn.cursor()
    data_samples = {}

    # fetch table names
    for table in entities:
        table_df = pd.read_sql("SELECT * FROM {} LIMIT {}".format(table, limit), conn)
        data_samples[table] = table_df.to_markdown(index=False)
    return data_samples


def get_gpt_sql_generation(question, keyword_annotations: str, operation_annotations: str, db_loc):
    # TODO: EXTRACT below code to a separate class/method and use it for operation annotation
    gpt_evidence_set = re.findall(r'`(.*?)`', keyword_annotations.split("Query:")[0])
    new_gpt_evidence_set = []

    for et in gpt_evidence_set:
        parentheses_match = re.match(r"\((.*?)\)", et)
        if parentheses_match is not None:
            new_gpt_evidence_set.append(parentheses_match.string)
        else:
            new_gpt_evidence_set.append(et)
    gpt_evidence_set = new_gpt_evidence_set

    gpt_evidence_set = set([e.split('.')[0] for e in gpt_evidence_set])

    table_names = get_db_tables(db_loc)
    new_gpt_evidence_set = []
    for et in gpt_evidence_set:
        try:
            new_gpt_evidence_set.append(process.extractOne(et, table_names, scorer=fuzz.WRatio)[0])
        except:
            pass
    gpt_evidence_set = set(new_gpt_evidence_set)

    if len(gpt_evidence_set) == 0:
        gpt_evidence_set = table_names

    ds = get_db_entities_samples(db_loc, gpt_evidence_set)
    sch = get_db_schema(db_loc, gpt_evidence_set)
    print(question)
    print(db_loc)
    print(sch)
    print(ds)


    keyword_annotations = keyword_annotations.split("Query:")[0]
    operation_annotations = operation_annotations.split("Query:")[0]
    # # message_context = [
    # #     {
    # #         "role": "system",
    # #         "content": "As a SQL Query GPT, your primary function is to assist users to write SQL queries for the given question, user's Keyword Annotations and user's Operation Annotations. You should respond to queries with clear SQL queries. \n\nAll database elements required to write the SQL query, excluding primary key-foreign key relationships, are given by the user. Therefore, avoid trying to figure out a  foreign key name for the JOIN clause because it often leadss to a SQLITE Error as such \"[1] [SQLITE_ERROR] SQL error or missing database (no such column: {column_in_your_response_JOIN_clause).\" The user knows how to write JOIN...ON clause  for your response and will replace the JOIN...ONclause with his own implementation.\n\nSpecify all columns in the SELECT statement with table namess to make sure the response doesn't return a SQLITE ERROR as such as f\"[[SQLITE_ERROR] SQL error or missing database (ambiguous column name:  {column_in_your_response}\"\n\nIn your responses, prioritize accuracy and relevance. \n\n"
    # #     },
    # #     {
    # #         "role": "user",
    # #         "content": f"Given the question, keyword annotations, and operation annotation\nto answer the question correctly. \n\nQuestion:\n{question}\n\nKeyword Annotations:\n{keyword_annotations}\n\nOperation Annotations:\n{operation_annotations}\n\nSQL Query:"
    # #     },
    # # ]
    # message_context = [
    #     {
    #         "role": "system",
    #         "content": "As a SQL Query GPT, your primary function is to assist users to write SQL queries for the given question, user's Keyword Annotations, and user's Operation Annotations.  The existence of nested query in the SQL query will be given as well. You should respond to questions with clear SQL queries.\nDO NO INTRODUCE or USE SQL Aliases in your response because it leads to a SQLITE Error as such \"[1] [SQLITE_ERROR] SQL error or missing database (no such column\" with the user's downstream pipeline.  \nInstead, specify all columns in the statement with table namess to make sure the response doesn't return a SQLITE ERROR as such as f\"[[SQLITE_ERROR] SQL error or missing database (ambiguous column name:  {column_in_your_response}.\" There's plenty of tokens.\nAll database elements required in the SQL query are provided by the user.\nIn your responses, prioritize accuracy and relevance. \n\n"
    #     },
    #     {
    #         "role": "user",
    #         "content": f"Given the question, keyword annotations, and operation annotation\nto answer the question correctly. \n\nQuestion:\n{question}\n\nKeyword Annotations:\n{keyword_annotations}\n\nOperation Annotations:\n{operation_annotations}\n\nExistence of Nested Query:\n{use_nested_query}\n\nSQL Query:"
    #     }
    # ]
    # completed = False
    #
    # while not completed:
    #     try:
    #         chat_completion = openai.ChatCompletion.create(
    #
    #             model="gpt-3.5-turbo",
    #             temperature=0.2,
    #             top_p=0.1,
    #             frequency_penalty=0,
    #             presence_penalty=0,
    #             messages=message_context,
    #             api_key="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"
    #         )
    #         completed = True
    #     except openai.error.RateLimitError:
    #         print("get_database_description - openai.error.RateLimitError")
    #         time.sleep(120)
    #
    # message_context += [{"role": "assistant", "content": chat_completion.choices[0].message.content}]
    # return chat_completion.choices[0].message.content


def generate_sqls(db_id):
    pbar = df[df['db_id'] == db_id].iterrows()
    conn = get_db_conn()
    db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")

    for i, r in pbar:
        # pbar.set_description(f"{db_id} - {i}")
        question = r['question']
        keyword_annotations = r['keyword_annotations']
        operation_annotations = r['operation_annotations']
        use_nested_query = r['use_nested_query']

        # sql_query = r['final_SQL_output']
        sql_generated = get_gpt_sql_generation(question, keyword_annotations, operation_annotations,
                                               # use_nested_query,
                                               db_loc
                                               )
        break
        # with conn.cursor() as cur:
        #     cur.execute(f"""INSERT INTO gatech."spider_dataset_output_sql_query"(db_id, question, gpt_output)
        #              VALUES (%s, %s,%s);""", (db_id, question, sql_generated,))
        #     conn.commit()
        #     cur.close()

    conn.close()


def extract_es_in_sql(query, db_loc):
    # Parse the query
    parsed = sqlparse.parse(query)[0]
    lower_to_correct_dict = get_lower_to_correct_dict(db_loc)
    table_names = [k for k in lower_to_correct_dict.keys() if len(k.split(".")) == 1]

    final_new_tokens = ""

    modifications = []

    # Iterate through the tokens in the parsed query

    # def extract(input_token, start_idx=0):
    es = []

    def extract(parsed):
        new_tokens = ""

        i = 0
        while i < len(parsed.tokens):
            token = parsed.tokens[i]
            if token.ttype is sqlparse.tokens.Keyword and token.value.upper() == 'FROM':
                #     # print(token)
                #     # Get the index of this token
                from_index = parsed.token_index(token) + 1
                to_idx = from_index
                for t in parsed.tokens[from_index:]:
                    to_idx += 1
                    if t.ttype is sqlparse.tokens.Keyword:
                        if t.value.upper() == 'ON' or 'JOIN' in t.value.upper():
                            pass
                        else:
                            break
                for next_token in parsed.tokens[from_index:to_idx - 2]:
                    if isinstance(next_token, sqlparse.sql.Identifier):
                        try:
                            es.append(
                                lower_to_correct_dict[difflib.get_close_matches(next_token.value, table_names)[0]])
                        except:
                            es.append(next_token.value)
                    elif isinstance(next_token, sqlparse.sql.IdentifierList):
                        for e in next_token.get_identifiers():
                            try:
                                es.append(
                                    lower_to_correct_dict[
                                        difflib.get_close_matches(e.value, table_names)[0]])
                            except:
                                es.append(e.value)

                # print(es)
                print(i, to_idx, len(parsed.tokens))
                i = to_idx - 2
            elif token.is_group:
                new_tokens += extract(token)
                i += 1
            else:
                new_tokens += token.value
                i += 1
        return new_tokens

    extract(parsed)
    # Return the original query if 'FROM' clause is not found
    return es



def modify_from_clause(query, db_loc):
    # Parse the query
    parsed = sqlparse.parse(query)[0]
    lower_to_correct_dict = get_lower_to_correct_dict(db_loc)
    table_names = [k for k in lower_to_correct_dict.keys() if len(k.split(".")) == 1]

    final_new_tokens = ""

    modifications = []

    # Iterate through the tokens in the parsed query

    # def extract(input_token, start_idx=0):
    def extract(parsed):
        new_tokens = ""

        i = 0
        while i < len(parsed.tokens):
            token = parsed.tokens[i]
            if token.ttype is sqlparse.tokens.Keyword and token.value.upper() == 'FROM':
            #     # print(token)
            #     # Get the index of this token
                from_index = parsed.token_index(token) + 1
                to_idx = from_index
                for t in parsed.tokens[from_index:]:
                    to_idx += 1
                    if t.ttype is sqlparse.tokens.Keyword:
                        if t.value.upper() == 'ON' or 'JOIN' in t.value.upper():
                            pass
                        else:
                            break
                es = []
                for next_token in parsed.tokens[from_index:to_idx - 2]:
                    if isinstance(next_token, sqlparse.sql.Identifier):
                        try:
                            es.append(
                                lower_to_correct_dict[difflib.get_close_matches(next_token.value, table_names)[0]])
                        except:
                            es.append(next_token.value)
                    elif isinstance(next_token, sqlparse.sql.IdentifierList):
                        for e in next_token.get_identifiers():
                            try:
                                es.append(
                                    lower_to_correct_dict[
                                        difflib.get_close_matches(e.value, table_names)[0]])
                            except:
                                es.append(e.value)

            # print(es)
                new_tokens += join_path_generator(db_loc, es) + " "
                print(i, to_idx, len(parsed.tokens))
                i = to_idx - 2
            elif token.is_group:
                new_tokens += extract(token)
                i += 1
            else:
                new_tokens += token.value
                i += 1
        return new_tokens
    final_new_tokens = extract(parsed)
    # Return the original query if 'FROM' clause is not found
    return final_new_tokens


def steiner_tree(sql_query_string, db_id):
    # print(db_id, sql_query_string)
    db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")
    try:
        modified_query = modify_from_clause(sql_query_string, db_loc)
        return modified_query
    except:
        return sql_query_string


def main(each_database_operation, persistence_db_table_name):
    global df

    existing_db = pd.read_sql(f"select * from {persistence_db_table_name}", get_db_conn())

    new_df = pd.merge(df, existing_db, how="outer", indicator=True,
                      left_on=['db_id', 'question'], right_on=['db_id', 'question']) \
        .query('_merge=="left_only"').drop('_merge', axis=1)
    df = new_df
    print(new_df)
    unique_db_ids = list(df['db_id'].unique())
    print(df)
    if len(unique_db_ids) == 0:
        print("Operation Complete.")
        return
    with tqdm.tqdm(total=len(unique_db_ids)) as pbar:
        with ThreadPoolExecutor(max_workers=4) as ex:
            futures = [ex.submit(each_database_operation, db_id) for db_id in unique_db_ids[:1]]
            for future in as_completed(futures):
                result = future.result()
                pbar.update(1)


def insert_graded_final_sql_query_to_db():
    persistence_db_table_name = 'gatech."spider_dataset_output_sql_query"'
    conn = get_db_conn()
    new_df = pd.read_sql(f"select * from {persistence_db_table_name}", conn)
    print(new_df)
    cursor = conn.cursor()
    for _, r in new_df.iterrows():
        extracted_sql_query = steiner_tree(r['gpt_output'], r['db_id'])
        cursor.execute(f"""
        UPDATE {persistence_db_table_name}
        SET steiner_postprocessed = %s
        WHERE db_id = %s and question = %s
        """, (extracted_sql_query, r['db_id'], r['question'],))
    conn.commit()
    cursor.close()
    conn.close()


if __name__ == '__main__':
    # sample_question = "Show the official names of the cities that have hosted more than one competition."
    # sample_input_sql_query = "SELECT COUNT(COMPETITION_RECORD.Competition_ID) > 1 FROM city JOIN farm_competition ON city.City_ID = farm_competition.Host_city_ID JOIN competition_record ON farm_competition.Competition_ID = competition_record.Competition_ID"
    # llm_output = get_sql_correction(sample_question, sample_input_sql_query)

    # print(llm_output)
    Fire({
        'generate_sqls': lambda: main(generate_sqls,
                                          'gatech."spider_dataset_output_sql_query"'),
        'steiner': lambda :insert_graded_final_sql_query_to_db()
    })

#     steiner_tree("""
# SELECT COUNT(DISTINCT College.state)
# FROM College
# JOIN Tryout ON College.enr = Tryout.enr
# WHERE Tryout.pPos = 'mid' AND College.state NOT IN (
#     SELECT College.state
#     FROM College
#     JOIN Tryout ON College.enr = Tryout.enr
#     WHERE Tryout.pPos = 'goalie'
# )
# """, "soccer_2")
#     insert_graded_final_sql_query_to_db()
