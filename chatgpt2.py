from langchain.prompts import ChatPromptTemplate
from langchain.chat_models import ChatOpenAI
from langchain.schema import AIMessage

model = ChatOpenAI(openai_api_key="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",
                   model_name="gpt-3.5-turbo", temperature=0)
#
#
# for


import pandas as pd


database_metadata_df = pd.read_json("tables.json")


def get_database_schema(db_id):
    db_metadata = database_metadata_df[database_metadata_df['db_id'] == db_id]
    # print(db_metadata)
    columns_dict = {}
    columns_type = list(db_metadata['column_types'])[0]

    for column, column_type in zip(list(db_metadata['column_names_original'])[0], columns_type):
        table_idx, column_name = column
        if table_idx == -1:
            continue
        if table_idx not in columns_dict.keys():
            columns_dict[table_idx] = []
        columns_dict[table_idx].append(column_name + ' ' + column_type)

    schema_string = ""
    for i, table in enumerate(list(db_metadata['table_names_original'])[0]):
        schema_string += table
        schema_string += " (" + ', '.join(columns_dict[i]) + ")"
        if i < len(list(db_metadata['table_names_original'])[0]) - 1:
            schema_string += '\n'

    return schema_string


def get_database_description(database_schema):



if __name__ == '__main__':
    print(get_database_schema('student_assessment'))
