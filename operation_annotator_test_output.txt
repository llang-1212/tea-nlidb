| Word      | Annotation |
|-----------|------------|
| What      |            |
| are       |            |
| the       |            |
| names     |            |
| of        |            |
| modern    |            |
| rooms     |            |
| that      |            |
| have      |            |
| a         |            |
| base      |            |
| price     |            |
| lower     | comparison |
| than      |            |
| $160      | comparison |
| and       |            |
| two       |            |
| beds      |            |
| .         |            |