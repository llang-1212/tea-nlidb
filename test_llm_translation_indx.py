import pandas as pd
from chatgpt import get_database_schema, get_database_description, get_evidence_set
import tqdm
from concurrent.futures import ThreadPoolExecutor, as_completed
import psycopg2


def get_db_conn():
    conn = psycopg2.connect("dbname='{db}' user='{user}' host='{host}' port='{port}' password='{passwd}'".format(
        user='postgres',
        passwd='',
        host="localhost",
        port=5432,
        db="postgres",
        options="-c search_path=8803,public"
    ))
    return conn


df = pd.read_csv('./Spider Dataset Weak Labels.csv')
existing_db = pd.read_sql("select * from gatech.\"spider_dataset_GPT_labels\"", get_db_conn())
unique_db_ids = list(df['Column1.db_id'].unique())


def each_database_operation(db_id):
    chatgpt_labels_db_description = []
    chatgpt_query_evidence_set = []

    database_schema = get_database_schema(db_id)
    # print(database_schema)
    database_description, message_context = get_database_description(database_schema)
    # print(database_description)
    chatgpt_labels_db_description.append({'db_id': db_id, 'database_description': database_description})

    # pbar = tqdm.tqdm(df[df['Column1.db_id'] == db_id].iterrows(), total=len(df[df['Column1.db_id'] == db_id]), position=)
    pbar = df[df['Column1.db_id'] == db_id].iterrows()
    conn = get_db_conn()
    for i, r in pbar:
        # pbar.set_description(f"{db_id} - {i}")
        question = r['Column1.question']
        evidence_set = get_evidence_set(message_context, question)
        with conn.cursor() as cur:
            cur.execute(f"""INSERT INTO gatech."spider_dataset_GPT_labels"(db_id, question, gpt_evidence_set) 
            VALUES (%s, %s,%s);""", (db_id, question, evidence_set,))
            conn.commit()
            cur.close()

        chatgpt_query_evidence_set.append({'db_id': db_id, 'question': question, 'GPT.Evidence_Set': evidence_set})
        # break

    conn.close()
    return chatgpt_labels_db_description, chatgpt_query_evidence_set


def main():
    global df

    new_df = pd.merge(df, existing_db, how="outer", indicator=True,
                      left_on=['Column1.db_id', 'Column1.question'], right_on=['db_id', 'question']) \
        .query('_merge=="left_only"').drop('_merge', axis=1)

    df = new_df
    llms_evidence_set = []

    chatgpt_labels_db_description = []
    chatgpt_query_evidence_set = []

    with tqdm.tqdm(total=len(unique_db_ids)) as pbar:
        with ThreadPoolExecutor(max_workers=len(unique_db_ids)) as ex:
            futures = [ex.submit(each_database_operation, db_id) for db_id in unique_db_ids]
            for future in as_completed(futures):
                result = future.result()
                desc, es = result
                print(desc, es)
                chatgpt_labels_db_description.extend(desc)
                chatgpt_query_evidence_set.extend(es)
                pbar.update(1)



    # # for db_id in unique_db_ids:
    # #     database_schema = get_database_schema(db_id)
    # #     database_description, message_context = get_database_description(database_schema)
    # #     chatgpt_labels_db_description.append({'db_id': db_id, 'database_description': database_description})
    # #
    # #     pbar = tqdm.tqdm(df[df['Column1.db_id'] == db_id].iterrows(), total=len(df[df['Column1.db_id'] == db_id]))
    # #     for i, r in pbar:
    # #         pbar.set_description(f"{db_id} - {i}")
    # #         question = r['Column1.question']
    # #         evidence_set = get_evidence_set(message_context, question)
    # #         chatgpt_query_evidence_set.append({'db_id': db_id, 'question': question, 'GPT.Evidence_Set': evidence_set})
    # #         break
    #
    # pd.DataFrame(chatgpt_labels_db_description).to_csv('Spider Dataset DB Description.csv')
    # pd.DataFrame(chatgpt_query_evidence_set).to_csv('Spider Dataset GPT Labels.csv')
    # print(pd.DataFrame(chatgpt_labels_db_description))


if __name__ == '__main__':
    main()
