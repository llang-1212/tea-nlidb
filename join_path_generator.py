from sys import maxsize

import networkx.exception

from fast_algorithm_for_steiner_trees_1981.kou_et_al import kou_et_al
import networkx as nx
import os

from process_sql import get_schema_and_fk_relationship


def distance_heuristics(nx_graph):
    length = dict(nx.all_pairs_dijkstra_path_length(nx_graph))

    def dist_function(n1, n2) -> int:
        try:
            return length[n1][n2]
        except KeyError:
            return maxsize
    return dist_function


def get_lower_to_correct_dict(db_location):
    sc, _ = get_schema_and_fk_relationship(db_location)
    lower_to_correct_dict = {}
    for (t, cols) in list(sc.items()):
        lower_to_correct_dict[t.lower()] = t
        for c in cols:
            entity_attribute = f"{t}.{c}"
            lower_to_correct_dict[entity_attribute.lower()] = entity_attribute


    return lower_to_correct_dict


def calculate_steiner_tree(db_location, evidence_set):
    sc, fkr = get_schema_and_fk_relationship(db_location)
    schema_graph = nx.Graph()


    for (t, cols) in list(sc.items()):
        schema_graph.add_node(t, type="entity")
        for c in cols:
            schema_graph.add_node(f"{t}.{c}", type="entity.attribute")
            schema_graph.add_edge(f"{t}.{c}", t, weight=1, type="entity.attribute")
            entity_attribute = f"{t}.{c}"
            if entity_attribute in fkr:
                for r in fkr[entity_attribute]:
                    schema_graph.add_edge(entity_attribute, r, weight=1, type="fkr")

    terminal_nodes = list(evidence_set)

    steiner_tree_approx = kou_et_al(schema_graph, distance_heuristics(schema_graph), terminal_nodes, "weight")
    return steiner_tree_approx

def split_entity_attribute(entity_attribute):
    entity, attribute = entity_attribute.split(".")
    return entity, attribute


def join_path_generator(db_location, evidence_set):
    sc, fkr = get_schema_and_fk_relationship(db_location)
    schema_graph = nx.Graph()


    for (t, cols) in list(sc.items()):
        schema_graph.add_node(t, type="entity")
        for c in cols:
            schema_graph.add_node(f"{t}.{c}", type="entity.attribute")
            schema_graph.add_edge(f"{t}.{c}", t, weight=1, type="entity.attribute")
            entity_attribute = f"{t}.{c}"
            if entity_attribute in fkr:
                for r in fkr[entity_attribute]:
                    schema_graph.add_edge(entity_attribute, r, weight=1, type="fkr")

    join_pairs_dict = {}
    for terminal_nodes in [(a, b) for idx, a in enumerate(evidence_set) for b in evidence_set[idx + 1:]]:
        try:

            steiner_tree_approx = kou_et_al(schema_graph, distance_heuristics(schema_graph), terminal_nodes, "weight")
        except networkx.exception.NetworkXNoPath:
            continue
        join_pairs = []
        edge_attributes = nx.get_edge_attributes(schema_graph, "type")
        for e in steiner_tree_approx.edges:
            if e in edge_attributes and edge_attributes[e] == "fkr":
                join_pairs.append((e[0], e[1]))
            elif (e[1], e[0]) in edge_attributes and edge_attributes[(e[1], e[0])] == "fkr":
                join_pairs.append((e[1], e[0]))

        for idx, jp in enumerate(join_pairs):
            entity_attribute_1, entity_attribute_2 = jp
            t_1, a_1 = split_entity_attribute(entity_attribute_1)
            t_2, a_2 = split_entity_attribute(entity_attribute_2)
            join_pairs_dict[(t_1, t_2)] = f'{t_1} and {t_2} has a foreign key relationship of {t_1} .{a_1} = {t_2}.{a_2}'
            join_pairs_dict[(t_2, t_1)] = f'{t_2} and {t_1} has a foreign key relationship of {t_2} .{a_2} = {t_1}.{a_1}'
    return join_pairs_dict


def foreign_key_path_generator(db_location, evidence_set):
    sc, fkr = get_schema_and_fk_relationship(db_location)
    schema_graph = nx.Graph()


    for (t, cols) in list(sc.items()):
        schema_graph.add_node(t, type="entity")
        for c in cols:
            schema_graph.add_node(f"{t}.{c}", type="entity.attribute")
            schema_graph.add_edge(f"{t}.{c}", t, weight=1, type="entity.attribute")
            entity_attribute = f"{t}.{c}"
            if entity_attribute in fkr:
                for r in fkr[entity_attribute]:
                    schema_graph.add_edge(entity_attribute, r, weight=1, type="fkr")

    terminal_nodes = list(evidence_set)
    try:
        steiner_tree_approx = kou_et_al(schema_graph, distance_heuristics(schema_graph), terminal_nodes, "weight")
    except networkx.exception.NodeNotFound:
        return []
    except networkx.exception.NetworkXNoPath:
        return  []
    edge_attributes = nx.get_edge_attributes(schema_graph, "type")
    join_pairs = []
    for e in steiner_tree_approx.edges:
        if e in edge_attributes and edge_attributes[e] == "fkr":
            join_pairs.append((e[0], e[1]))
        elif (e[1], e[0]) in edge_attributes and edge_attributes[(e[1], e[0])] == "fkr":
            join_pairs.append((e[1], e[0]))

    FROM_clause = []
    mentioned_tables = set()



    if len(join_pairs) == 0:
        return []
    else:
        for idx, jp in enumerate(join_pairs):
            entity_attribute_1, entity_attribute_2 = jp
            t_1, a_1 = split_entity_attribute(entity_attribute_1)
            t_2, a_2 = split_entity_attribute(entity_attribute_2)

            FROM_clause.append(f"{t_1} JOIN {t_2} ON {entity_attribute_1} = {entity_attribute_2}")

    return FROM_clause

"""
Natural Language
Elucidated
By
Using
Llms
And
Rule-based systems
"""