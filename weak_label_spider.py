import pandas as pd
from sql_metadata import Parser
from sqlglot import parse_one, exp
import re

df = pd.read_csv('Spider Dataset.csv', encoding='unicode_escape')
print(df.columns)
print(len(df[df['Query Type'] == 'non-nested']))

aggregate_functions = ["SUM", "AVG", "MAX", "MIN", "COUNT"]

query_types = []
columns = []
for i, r in df.iterrows():
    sql_query = r['Column1.query']
    sql_parser = Parser(sql_query)
    sql_parser_2 = parse_one(sql_query)

    sql_columns = []

    try:
        if len(sql_parser.columns) == 0:
            sql_columns = sql_parser.tables
        else:
            d = sql_parser.columns_dict
            result = d.get('select', []) + d.get('where', []) + d.get('group_by', []) + d.get('having', []) + d.get('order_by', [])

            sql_columns = result
    except:
        columns.append([])
        query_types.append('un-labeled')
        continue

    for c in sql_parser_2.find_all(exp.Column):
        # print(str(c))
        c = str(c)
        # print(re.findall(r'["][\w\s]+["]', c))
        if len(re.findall(r'["][\w\s]+["]', c)) > 0:
            # print(c.replace('"', '') in sql_columns)
            sql_columns = [el for el in sql_columns if el != c.replace('"', '')]

    columns.append(sql_columns)
    query_types.append('un-labeled')
    # if len(sql_parser.tables) == 1:
    #     query_types.append('non-nested')
    # else:
    #     try:
    #         columns_dict = sql_parser.columns_dict
    #
    #         # print(sql_query)
    #         # select_columns = columns_dict['select']
    #         # for c in select_columns:
    #         #     c_split = c.split('.')
    #         #     if len(c_split) < 2:
    #         #         pass
    #                 # print(sql_query)
    #         # print(sql_parser.tables)
    #
    #         query_types.append('un-labeled')
    #     except AttributeError:
    #         query_types.append('un-labeled')



df['Query Types'] = query_types
df['Evidence Set'] = columns
print(len(df[df['Query Types'] == 'non-nested']))
df.to_csv('Spider Dataset Weak Labels V2.csv')