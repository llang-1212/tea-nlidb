Official implementation for paper TEA: Toward Enterprise Adaption of NLIDB with code, prompts, model outputs.



## Setup
1. Set up OpenAI API key and store in environment variable ``OPENAI_API_KEY`` 
2. Set up Python environment
```bash
pip install -r requirements.txt
```