import re
import time
from concurrent.futures import ThreadPoolExecutor, as_completed

import openai
import pandas as pd
import psycopg2
import tqdm
from fire import Fire

df = pd.read_csv('./NEBULAR-pipeline-231126-output.csv')


def get_db_conn():
    conn = psycopg2.connect("dbname='{db}' user='{user}' host='{host}' port='{port}' password='{passwd}'".format(
        user='postgres',
        passwd='ilovetony',

        host="localhost",
        port=5432,
        db="postgres",
        options="-c search_path=8803,public"
    ))
    return conn


def get_sql_correction(question, sql_query):
    message_context = [
        {
            "role": "user",
            "content": f"Grade the input SQL query for the given question and then, reply with a corrected version of the input SQL query.  If there are no errors, reply with a copy of the original SQL query.  All database elements required are in the SQL query.\n\nQuestion:\n{question}\n\nInput SQL Query:\n{sql_query}\n\nCorrected SQL Query:"
        }
    ]
    completed = False

    while not completed:
        try:
            chat_completion = openai.ChatCompletion.create(

                model="gpt-3.5-turbo",
                temperature=0.2,
                top_p=0.1,
                frequency_penalty=0,
                presence_penalty=0,
                messages=message_context,
                api_key="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"
            )
            completed = True
        except openai.error.RateLimitError:
            print("get_database_description - openai.error.RateLimitError")
            time.sleep(120)

    message_context += [{"role": "assistant", "content": chat_completion.choices[0].message.content}]
    return chat_completion.choices[0].message.content


def generate_gpt_revisions(db_id):
    pbar = df[df['db_id'] == db_id].iterrows()
    conn = get_db_conn()

    for i, r in pbar:
        # pbar.set_description(f"{db_id} - {i}")
        question = r['question']
        sql_query = r['final_SQL_output']
        sql_correction = get_sql_correction(question, sql_query)
        with conn.cursor() as cur:
            cur.execute(f"""INSERT INTO gatech."spider_dataset_GPT_final_sql_correction"(db_id, question, gpt_output)
                     VALUES (%s, %s,%s);""", (db_id, question, sql_correction,))
            conn.commit()
            cur.close()

    conn.close()


def extract_complete_sql_query(input_string):
    # Revised regular expression pattern to extract the entire SQL query
    pattern = r"SELECT.*?(?=\n\n|\Z)"

    # Searching for the SQL query in the input string
    match = re.search(pattern, input_string, re.DOTALL | re.IGNORECASE)

    # Return the matched SQL query if found, else return None
    return match.group(0).strip() if match else None


def insert_original_sql_query_to_db():
    persistence_db_table_name = 'gatech."spider_dataset_GPT_final_sql_correction"'
    global df
    existing_db = pd.read_sql(f"select * from {persistence_db_table_name}", get_db_conn())
    new_df = pd.merge(df, existing_db, how="outer", indicator=True,
                      left_on=['db_id', 'question'], right_on=['db_id', 'question'])\
        .query('_merge=="both"').drop('_merge', axis=1)
    print(new_df['final_SQL_output'])
    conn = get_db_conn()
    cursor = conn.cursor()
    for _, r in new_df.iterrows():
        cursor.execute(f"""
        UPDATE {persistence_db_table_name}
        SET original_final_sql_query = %s
        WHERE db_id = %s and question = %s 
        """, (r['final_SQL_output'], r['db_id'], r['question'],))
    conn.commit()
    cursor.close()
    conn.close()


def insert_graded_final_sql_query_to_db():
    persistence_db_table_name = 'gatech."spider_dataset_GPT_final_sql_correction"'
    conn = get_db_conn()
    new_df = pd.read_sql(f"select * from {persistence_db_table_name}", conn)
    print(new_df)
    cursor = conn.cursor()
    for _, r in new_df.iterrows():
        extracted_sql_query = extract_complete_sql_query(r['gpt_output'])
        if extracted_sql_query is None:
            extracted_sql_query = r['original_final_sql_query']
        cursor.execute(f"""
        UPDATE {persistence_db_table_name}
        SET graded_final_sql_query = %s
        WHERE db_id = %s and question = %s
        """, (extracted_sql_query, r['db_id'], r['question'],))
    conn.commit()
    cursor.close()
    conn.close()


def main(each_database_operation, persistence_db_table_name):
    global df

    existing_db = pd.read_sql(f"select * from {persistence_db_table_name}", get_db_conn())

    new_df = pd.merge(df, existing_db, how="outer", indicator=True,
                      left_on=['db_id', 'question'], right_on=['db_id', 'question']) \
        .query('_merge=="left_only"').drop('_merge', axis=1)
    df = new_df

    unique_db_ids = list(df['db_id'].unique())

    if len(unique_db_ids) == 0:
        print("Operation Complete.")
        return
    with tqdm.tqdm(total=len(unique_db_ids)) as pbar:
        with ThreadPoolExecutor(max_workers=len(unique_db_ids)) as ex:
            futures = [ex.submit(each_database_operation, db_id) for db_id in unique_db_ids]
            for future in as_completed(futures):
                result = future.result()
                pbar.update(1)


if __name__ == '__main__':
    # sample_question = "Show the official names of the cities that have hosted more than one competition."
    # sample_input_sql_query = "SELECT COUNT(COMPETITION_RECORD.Competition_ID) > 1 FROM city JOIN farm_competition ON city.City_ID = farm_competition.Host_city_ID JOIN competition_record ON farm_competition.Competition_ID = competition_record.Competition_ID"
    # llm_output = get_sql_correction(sample_question, sample_input_sql_query)
    #
    # print(llm_output)
    Fire({
        'revise_final_sql_output': lambda: main(generate_gpt_revisions,
                                          'gatech."spider_dataset_GPT_final_sql_correction"'),
        'insert_original_sql_query_to_db': lambda : insert_original_sql_query_to_db(),
        'insert_graded_final_sql_query_to_db': lambda : insert_graded_final_sql_query_to_db()
    })