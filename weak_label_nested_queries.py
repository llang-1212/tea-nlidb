import os

import sqlparse
import sqlparse.sql
from fire import Fire

from process_sql import Schema, get_schema, get_sql
from spider_difficulty_evaluation import eval_hardness


def weak_label_function(sql_query_string):
    parsed_sql_query = sqlparse.parse(sql_query_string)[0]

    outer_query_identifiers = []
    subqueries = []

    def extract(tokenList, level=0):
        for token in tokenList:
            # print(level, type(token), token.within(sqlparse.sql.Function), token)
            # print(token.get_type())
            if isinstance(token, sqlparse.sql.Where) and level == 0:
                for t in token:
                    if isinstance(t, sqlparse.sql.Identifier):
                        outer_query_identifiers.append(t.value)

            if isinstance(token, sqlparse.sql.Identifier) and level == 0:  # SELECT or FROM
                outer_query_identifiers.append(token.value.split(' ')[0])
            elif isinstance(token, sqlparse.sql.IdentifierList) and level == 0:  # SEELECT or FROM
                for t in token:
                    # print('it', type(t))
                    if isinstance(t, sqlparse.sql.Identifier):
                        outer_query_identifiers.append(t.value)
                    elif isinstance(t, sqlparse.sql.Function):
                        for e in t:
                            if isinstance(e, sqlparse.sql.Parenthesis):
                                for it in e.tokens:
                                    if isinstance(it, sqlparse.sql.Identifier):
                                        outer_query_identifiers.append(it.value)

            elif not token.within(sqlparse.sql.Function) and isinstance(token, sqlparse.sql.Parenthesis):
                subqueries.append(token.value)

            elif token.is_group and not isinstance(token, sqlparse.sql.Parenthesis):
                extract(token.tokens, level + 1)

    extract(parsed_sql_query.tokens)

    return list(set(outer_query_identifiers)), list(set(subqueries))


def label_spider():
    import pandas as pd
    from tqdm import tqdm
    df = pd.read_csv('Spider Dataset.csv', encoding='unicode_escape')

    df_outer_query_identifiers = []
    df_subqueries = []
    df_num_subqueries = []
    df_nested_query_type = []
    for i, r in tqdm(df.iterrows(), total=len(df)):
        sql_query = r['Column1.query']
        outer_query_identifiers, subqueries = weak_label_function(sql_query)
        df_outer_query_identifiers.append(outer_query_identifiers)
        df_subqueries.append(subqueries)
        df_num_subqueries.append(len(subqueries))
        if len(subqueries) == 0:
            df_nested_query_type.append('NA')
        else:
            df_nested_query_type.append('TYPE-X')
    df['outer_query_identifiers'] = df_outer_query_identifiers
    df['inner_queries'] = df_subqueries
    df['num_inner_queries'] = df_num_subqueries
    # Label difficulties
    db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')
    difficulty_labels = []
    for _, r in df.iterrows():
        # print(eval_hardness(get_sql(r)))
        sql_string = r['Column1.query']
        db_id = r['Column1.db_id']
        db = os.path.join(db_dir, db_id, db_id + ".sqlite")
        try:
            db_schema = Schema(get_schema(db))
            g_sql = get_sql(db_schema, sql_string)
            difficulty_labels.append(eval_hardness(g_sql))
        except:
            difficulty_labels.append('error')
    df['difficulty'] = difficulty_labels

    df['nested_query_type'] = df_nested_query_type

    df.to_csv('Spider Dataset Weak Labels NQ.csv')


if __name__ == '__main__':
    Fire({
        'label_spider': label_spider
    })
