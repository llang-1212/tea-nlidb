import os
import time
import pandas as pd
import openai

from operation_annotator import represent_schema_od

db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')

database_metadata_df = pd.read_json("tables.json")


def get_database_schema(db_id, return_table_dict=False, return_table_dict_w_table_name=False):
    db_metadata = database_metadata_df[database_metadata_df['db_id'] == db_id]
    # print(db_metadata)
    columns_dict = {}
    columns_dict_w_table_names = {}
    columns_type = list(db_metadata['column_types'])[0]
    table_names = list(db_metadata['table_names_original'])[0]
    for column, column_type in zip(list(db_metadata['column_names_original'])[0], columns_type):
        table_idx, column_name = column
        if table_idx == -1:
            continue
        if table_idx not in columns_dict.keys():
            columns_dict[table_idx] = []
            columns_dict_w_table_names[table_names[table_idx].lower()] = {}
        columns_dict[table_idx].append(column_name + ' ' + column_type)
        columns_dict_w_table_names[table_names[table_idx].lower()][column_name.lower()] = column_type

    if return_table_dict:
        return columns_dict

    if return_table_dict_w_table_name:
        return columns_dict_w_table_names

    schema_string = ""
    for i, table in enumerate(list(db_metadata['table_names_original'])[0]):
        schema_string += table
        schema_string += " (" + ', '.join(columns_dict[i]) + ")"
        if i < len(list(db_metadata['table_names_original'])[0]) - 1:
            schema_string += '\n'

    return schema_string


def process_one_record(inst):
    db_id = inst['db_id']
    db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")

    nested_query_detected = inst['nqt_len'] > 0
    token_annotations_df = pd.DataFrame(inst['token_annotations'])
    token_annotations_df.rename({'characters': 'token'}, axis=1, inplace=True)
    question = inst['question']
    # table_names = get_db_tables(db_loc)
    # sch = get_db_schema(db_loc, table_names)

if __name__ == '__main__':
    database_schema = get_database_schema('customers_card_transactions', return_table_dict_w_table_name=True)

    print(represent_schema_od(database_schema))
    # print(operation_annotation)
    # with open('chatgpt_response.txt', 'w') as f:
    #     f.write(database_description)
    #
    # evidence_set = get_evidence_set(message_context)
    # # evidence_set = get_evidence_set(database_description)
    # #
    # with open('chatgpt_evidence_set_response.txt', 'a+') as f:
    #     f.write(evidence_set)