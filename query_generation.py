import sqlite3
from concurrent.futures import ThreadPoolExecutor, as_completed
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

import pandas as pd
import re
import ast
import os
import json

import tqdm

import configs
from chatgpt import get_database_schema
from join_path_generator import calculate_steiner_tree, foreign_key_path_generator, join_path_generator
from llm_then_steiner import get_db_tables, get_db_schema, extract_es_in_sql
from nested_query_annotator import extract_instance_token
from operation_annotator import represent_schema, represent_schema_od
from test_llm_translation_indx import get_db_conn
from spider_difficulty_evaluation import eval_hardness
from process_sql import get_sql, get_schema, Schema
import openai

# weak_column_labels = pd.read_csv('Spider Dataset Weak Labels V2.csv')
weak_column_labels = pd.read_csv('Spider Dataset Weak Labels NQ.csv')
db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')


def json_string_to_pandas(json_string):
    try:
        return pd.read_json(json.loads(json_string))
    except:
        return pd.DataFrame(json.loads(json_string))


# Define a custom function to combine columns
def combine_table_and_column(row):
    if row['related_database_column'] is not None:
        return f"{row['related_database_table']}.{row['related_database_column']}"
    elif row['related_database_table'] is not None:
        return f"{row['related_database_table']}"
    else:
        return None


def local_code_interpreter(db_loc, p_str):
    conn = sqlite3.connect(db_loc)
    cursor = conn.cursor()
    p_str_executed = False
    try:
        cursor.execute(p_str)
        p_res = cursor.fetchall()
        p_str_executed = True
    except Exception as e:
        return str(e), p_str_executed
    return p_res, p_str_executed


def create_list_of_evidence_set(annotation_df):
    found_instances = set()

    for i, a in annotation_df.iterrows():
        column, table = a['related_database_column'], a['related_database_table']
        if table is not None:
            if column is not None:
                found_instances.add(f'{table}.{column}')
            found_instances.add(f'{table}')
    return list(found_instances)


def create_list_of_instance_values(annotation_df):
    found_instances = set()

    for i, a in annotation_df.iterrows():
        h = a['raw_operation_annotation']
        if h is None:
            continue
        found_instance_values = extract_instance_token(h)
        if found_instance_values is not None:
            for found_instance_value in found_instance_values:
                found_instances.add((found_instance_value, combine_table_and_column(a)))

    return list(found_instances)


def find_column_name(text):
    # Regular expression pattern
    pattern = r"no such column: (\S+)"

    # Search for the pattern in the text
    match = re.search(pattern, text)

    # If a match is found, return the captured group
    if match:
        return match.group(1)
    else:
        return None


def improve_using_local_code_interpreter(db_id, db_loc, table_names, sql_query, gpt_message_context=[]):
    local_code_interpreter_output, ran_successfully = local_code_interpreter(db_loc, sql_query)
    if not ran_successfully:
        join_paths = join_path_generator(db_loc, table_names)
        if 'no such column' in local_code_interpreter_output:
            confused_column = find_column_name(local_code_interpreter_output)
            confused_column_name = confused_column.split('.')[0]
            column_names = list(get_db_schema(db_id, table_names).values())
            es = extract_es_in_sql(sql_query, db_loc)
            print("confused column", confused_column)
            print("column_names", column_names)
            if confused_column.lower() in [c.lower() for c in column_names]:
                # not hallucinating columns
                feedback = f"Problem:\n{confused_column_name} is in {','.join([c.lower() for c in column_names])}.\nWays to improve answer:\n"
                for cc in [c for c in column_names if confused_column.lower() in c.lower()]:
                    for e in es:
                        if (cc, e) in join_paths.keys():
                            feedback += join_paths[cc, e] + "\n"
            else:
                feedback = local_code_interpreter_output
        else:
            feedback = local_code_interpreter_output

        gpt_message_context.append({
            "role": "user",
            "content": feedback + "\n# SQL Query:"
        })

        chat_completion = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=gpt_message_context,
            temperature=0.2,
            top_p=0.1,
            frequency_penalty=0,
            presence_penalty=0,
            api_key=configs.openai_api_key
        )
        gpt_output = chat_completion.choices[0].message.content
        new_sql_query = gpt_output
    else:
        new_sql_query = sql_query
    return new_sql_query


def process_one_record(inst):
    db_id = inst['db_id']
    db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")

    nested_query_detected = inst['nqt_len'] > 0
    token_annotations_df = pd.DataFrame(inst['token_annotations'])
    token_annotations_df['database element'] = token_annotations_df.apply(combine_table_and_column, axis=1)
    token_annotations_df.rename({'characters': 'token'}, axis=1, inplace=True)
    if nested_query_detected:
        nested_query_annotations_df = pd.DataFrame(inst['nested_query_annotations'])
        # print(nested_query_annotations_df)
        for _, nqt_candidate in nested_query_annotations_df.iterrows():
            outer_evidence_set = json_string_to_pandas(nqt_candidate['outer_query'])
            inner_evidence_set = json_string_to_pandas(nqt_candidate['inner_query'])
            outer_join_condition = json_string_to_pandas(nqt_candidate['join_condition_before'])
            inner_join_condition = json_string_to_pandas(nqt_candidate['join_condition_after'])

            inner_list_of_evidence_set = create_list_of_evidence_set(inner_join_condition)

            if len(inner_list_of_evidence_set) == 0:
                db_id = inst['db_id']

                db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")

                token_annotations_df = pd.DataFrame(inst['token_annotations'])
                token_annotations_df.rename({'characters': 'token'}, axis=1, inplace=True)
                question = inst['question']

                table_names = get_db_tables(db_loc)
                sch = get_db_schema(db_loc, table_names)

                list_of_evidence_set = create_list_of_evidence_set(token_annotations_df)
                evidence_set_prompt = "The SQL query must contain " + ', '.join(list_of_evidence_set)
                list_of_instance_values = create_list_of_instance_values(token_annotations_df)
                instance_value_prompts = []
                for li in list_of_instance_values:
                    instance_value_prompts.append(f"'{li[0]}' that is related to {li[1]}")
                instance_value_prompt = "In terms of instances, the SQL query must contain " + ', '.join(
                    instance_value_prompts)

                user_prompt = f"\n### Complete sqlite SQL query only and with explanation\n### SQLite SQL tables, with their properties:\n{represent_schema_od(sch)}### {question}\n#\n### {evidence_set_prompt}\n"
                if len(instance_value_prompts) > 0:
                    user_prompt += "\n#\n### " + instance_value_prompt
                user_prompt += "SELECT"
                gpt_message_context = [
                    {
                        "role": "user",
                        "content": user_prompt
                    }
                ]

                chat_completion = openai.ChatCompletion.create(
                    model="gpt-3.5-turbo",
                    messages=gpt_message_context,
                    temperature=0.2,
                    top_p=0.1,
                    frequency_penalty=0,
                    presence_penalty=0,
                    api_key=configs.openai_api_key
                )

                gpt_output = chat_completion.choices[0].message.content
                gpt_message_context.append({
                    "role": "assistant",
                    "content": gpt_output
                })
                gpt_output = 'SELECT ' + gpt_output.split(";")[0].split("Explanation:")[0]
                local_code_interpreter_output, ran_successfully = local_code_interpreter(db_loc, gpt_output)
                if not ran_successfully:
                    join_paths = join_path_generator(db_loc, table_names)
                    if 'no such column' in local_code_interpreter_output:
                        confused_column = find_column_name(local_code_interpreter_output)
                        confused_column_name = confused_column.split('.')[0]
                        column_names = list(get_db_schema(db_id, table_names).values())
                        es = extract_es_in_sql(gpt_output, db_loc)
                        print("confused column", confused_column)
                        print("column_names", column_names)
                        if confused_column.lower() in [c.lower() for c in column_names if isinstance(c, str)]:
                            # not hallucinating columns
                            feedback = f"Problem:\n{confused_column_name} is in {','.join([c.lower() for c in column_names])}.\nWays to improve answer:\n"
                            for cc in [c for c in column_names if confused_column.lower() in c.lower()]:
                                for e in es:
                                    if (cc, e) in join_paths.keys():
                                        feedback += join_paths[cc, e] + "\n"
                        else:
                            feedback = local_code_interpreter_output
                    else:
                        feedback = local_code_interpreter_output

                    gpt_message_context.append({
                        "role": "user",
                        "content": feedback + "\n# SQL Query:"
                    })
                    print("debug message context")
                    print(gpt_message_context)

                    chat_completion = openai.ChatCompletion.create(
                        model="gpt-3.5-turbo",
                        messages=gpt_message_context,
                        temperature=0.2,
                        top_p=0.1,
                        frequency_penalty=0,
                        presence_penalty=0,
                        api_key=configs.openai_api_key
                    )
                    gpt_output = chat_completion.choices[0].message.content
                continue
            else:
                return
                question = inst['question']

                inner_question = ' '.join(inner_evidence_set['characters'].tolist())
                outer_question = ' '.join(outer_evidence_set['characters'].tolist())
                table_names = get_db_tables(db_loc)
                sch = get_db_schema(db_loc, table_names)
                inner_user_prompt = f"\n### Complete sqlite SQL query only and with explanation\n### SQLite SQL tables, with their properties:\n{represent_schema_od(sch)}### {inner_question}\nSELECT"
                outer_user_prompt = f"\n### Complete sqlite SQL query only and with explanation\n### SQLite SQL tables, with their properties:\n{represent_schema_od(sch)}### {outer_question}\nSELECT"

                inner_gpt_message_context = [
                    {
                        "role": "user",
                        "content": inner_user_prompt
                    }
                ]
                inner_chat_completion = openai.ChatCompletion.create(
                    model="gpt-3.5-turbo",
                    messages=inner_gpt_message_context,
                    temperature=0.2,
                    top_p=0.1,
                    frequency_penalty=0,
                    presence_penalty=0,
                    api_key=configs.openai_api_key
                )

                inner_gpt_output = inner_chat_completion.choices[0].message.content
                inner_gpt_message_context.append({
                    "role": "assistant",
                    "content": inner_gpt_output
                })

                inner_sql = improve_using_local_code_interpreter(db_id, db_loc, table_names,
                                                                 sql_query='SELECT ' +
                                                                           inner_gpt_output.split(";")[0].split(
                                                                               "Explanation:")[0],
                                                                 gpt_message_context=inner_gpt_message_context
                                                                 )

                outer_gpt_message_context = [
                    {
                        "role": "user",
                        "content": outer_user_prompt
                    }
                ]
                outer_chat_completion = openai.ChatCompletion.create(
                    model="gpt-3.5-turbo",
                    messages=outer_gpt_message_context,
                    temperature=0.2,
                    top_p=0.1,
                    frequency_penalty=0,
                    presence_penalty=0,
                    api_key=configs.openai_api_key
                )

                outer_gpt_output = outer_chat_completion.choices[0].message.content
                outer_gpt_message_context.append({
                    "role": "assistant",
                    "content": outer_gpt_output
                })
                outer_sql = improve_using_local_code_interpreter(db_id, db_loc, table_names,
                                                                 sql_query='SELECT ' +
                                                                           outer_gpt_output.split(";")[0].split(
                                                                               "Explanation:")[0],
                                                                 gpt_message_context=inner_gpt_message_context
                                                                 )
                print(outer_question, " | ", inner_question, "\n", inner_sql, outer_sql)
                join_user_prompt = f"\n###  Complete sqlite SQL query only and with explanation\n### SQLite SQL tables, with their properties:\n{represent_schema_od(sch)}\n### Inner SQLite query, with their question:#\n#{inner_sql}#\n#{inner_question}\n#\n### Outer SQLite query, with their question:#\n#{outer_sql}#\n#{outer_question}\n#\n###{question}\nSELECT"
                join_gpt_message_context = [
                    {
                        "role": "user",
                        "content": join_user_prompt
                    }
                ]
                join_chat_completion = openai.ChatCompletion.create(
                    model="gpt-3.5-turbo",
                    messages=join_gpt_message_context,
                    temperature=0.2,
                    top_p=0.1,
                    frequency_penalty=0,
                    presence_penalty=0,
                    api_key=configs.openai_api_key
                )

                join_gpt_output = join_chat_completion.choices[0].message.content
                join_gpt_message_context.append({
                    "role": "assistant",
                    "content": join_gpt_output
                })
                gpt_output = improve_using_local_code_interpreter(db_id, db_loc, table_names,
                                                                 sql_query='SELECT ' +
                                                                           join_gpt_output.split(";")[0].split(
                                                                               "Explanation:")[0],
                                                                 gpt_message_context=join_gpt_message_context
                                                                 )
            # evidence_set_before.rename({'characters': 'token'}, axis=1, inplace=True)
            # evidence_set_after.rename({'characters': 'token'}, axis=1, inplace=True)
            # join_condition_before.rename({'characters': 'token'}, axis=1, inplace=True)
            # join_condition_after.rename({'characters': 'token'}, axis=1, inplace=True)
            #
            # evidence_set_before['database element'] = evidence_set_before.apply(combine_table_and_column, axis=1)
            # evidence_set_after['database element'] = evidence_set_after.apply(combine_table_and_column, axis=1)
            # join_condition_before['database element'] = join_condition_before.apply(combine_table_and_column, axis=1)
            # join_condition_after['database element'] = join_condition_after.apply(combine_table_and_column, axis=1)

            # evidence_set_before_question = ' '.join(evidence_set_before['token'].to_list())
            # TODO:
            # print("Question:\n", evidence_set_before_question)
            # print(evidence_set_before[['token', 'database element', 'operation_annotation']])
            # # -----------------
            # print(evidence_set_after[['token', 'database element', 'operation_annotation']])
    else:

        db_id = inst['db_id']

        db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")

        token_annotations_df = pd.DataFrame(inst['token_annotations'])
        token_annotations_df.rename({'characters': 'token'}, axis=1, inplace=True)
        question = inst['question']
        table_names = get_db_tables(db_loc)
        sch = get_db_schema(db_loc, table_names)

        list_of_evidence_set = create_list_of_evidence_set(token_annotations_df)
        evidence_set_prompt = "The SQL query must contain " + ','.join(list_of_evidence_set) + "."
        list_of_instance_values = create_list_of_instance_values(token_annotations_df)
        instance_value_prompts = []
        for li in list_of_instance_values:
            instance_value_prompts.append(f"'{li[0]}' that is related to {li[1]}")
        instance_value_prompt = "In terms of instances, the SQL query must contain " + ', '.join(instance_value_prompts) + "."


        user_prompt = f"\n### Complete sqlite SQL query only and with explanation\n### SQLite SQL tables, with their properties:\n{represent_schema_od(sch)}### {question}\n#\n### {evidence_set_prompt}\n"
        if len(instance_value_prompts) > 0:
            user_prompt += "\n#\n### " + instance_value_prompt
        user_prompt += "SELECT"
        gpt_message_context = [
            {
                "role": "user",
                "content": user_prompt
            }
        ]
        chat_completion = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=gpt_message_context,
            temperature=0.2,
            top_p=0.1,
            frequency_penalty=0,
            presence_penalty=0,
            api_key=configs.openai_api_key
        )

        gpt_output = chat_completion.choices[0].message.content
        gpt_message_context.append({
            "role": "assistant",
            "content": gpt_output
        })
        gpt_output = 'SELECT ' + gpt_output.split(";")[0].split("Explanation:")[0]
        local_code_interpreter_output, ran_successfully = local_code_interpreter(db_loc, gpt_output)
        if not ran_successfully:
            join_paths = join_path_generator(db_loc, table_names)
            if 'no such column' in local_code_interpreter_output:
                confused_column = find_column_name(local_code_interpreter_output)
                confused_column_name = confused_column.split('.')[0]
                column_names = list(get_db_schema(db_id, table_names).values())
                es = extract_es_in_sql(gpt_output, db_loc)
                print("confused column", confused_column)
                print("column_names", column_names)
                if confused_column.lower() in [c.lower() for c in column_names]:
                    # not hallucinating columns
                    feedback = f"Problem:\n{confused_column_name} is in {','.join([c.lower() for c in column_names])}.\nWays to improve answer:\n"
                    for cc in [c for c in column_names if confused_column.lower() in c.lower()]:
                        for e in es:
                            if (cc, e) in join_paths.keys():
                                feedback += join_paths[cc, e] + "\n"
                else:
                    feedback = local_code_interpreter_output
            else:
                feedback = local_code_interpreter_output

            gpt_message_context.append({
                "role": "user",
                "content": feedback + "\n# SQL Query:"
            })
            print("debug message context")
            print(gpt_message_context)

            chat_completion = openai.ChatCompletion.create(
                model="gpt-3.5-turbo",
                messages=gpt_message_context,
                temperature=0.2,
                top_p=0.1,
                frequency_penalty=0,
                presence_penalty=0,
                api_key=configs.openai_api_key
            )
            gpt_output = chat_completion.choices[0].message.content

    #     # print(token_annotations_df)
    #     question = []
    #     for _, ta in token_annotations_df.iterrows():
    #         # print(ta)
    #         token_string = ""
    #         token_string += ta['token']
    #         # if ta['database element'] is not None:
    #         #     token_string += f' [{ta["database element"]}]'
    #         question.append(token_string)
    #
    #     print("No nested query")
    #     question = ' '.join(question)
    #
    #     print("Question:\n", question)
    #
    #     # print(token_annotations_df[['token', 'database element', 'operation_annotation']].to_markdown())
    #     # print(token_annotations_df.columns)
    #     # value instances
    #
    #     list_of_evidence_set = create_list_of_evidence_set(token_annotations_df)
    #     # steiner tree
    # #
    #     foreign_key_joins_to_consider = foreign_key_path_generator(db_loc, list_of_evidence_set)
    #     found_list_of_instance_values = create_list_of_instance_values(token_annotations_df)
    #     # print('found_list_of_instance_values', found_list_of_instance_values)
    #     user_prompt = f"# Question:\n{question}\n\n# Annotation:\n{token_annotations_df[['token', 'database element']].to_markdown()}\n\n"
    #     if len(foreign_key_joins_to_consider) > 0:
    #         foreign_key_joins_string = '\n'.join(foreign_key_joins_to_consider)
    #         user_prompt += f"## Possible Joins: {foreign_key_joins_string}"
    #     if len(found_list_of_instance_values) > 0:
    #         instance_values_string = '\n'.join(found_list_of_instance_values)
    #         user_prompt += f"## Constants: {instance_values_string}"
    #
    #     table_names = get_db_tables(db_loc)
    #     sch = get_db_schema(db_loc, table_names)
    #
    #     user_prompt = f"\n### Complete sqlite SQL query only and with explanation\n### SQLite SQL tables, with their properties:\n{represent_schema_od(sch)}### {question}\nSELECT"
    #
    #     print(user_prompt)
    #     chat_completion = openai.ChatCompletion.create(
    #         model="gpt-3.5-turbo",
    #         messages=[
    #             {
    #                 "role": "system",
    #                 "content": "As a SQL Query Translator GPT, your primary function is to convert the query and its annotations, which are in the form of markdown table and  bullet points, into a sqlite SQL query over a database.\nFirst think step-by-step.\nThen, output the code in a single code block.\nMinimize any other prose.\n\nDO NO INTRODUCE or USE SQL Aliases in your response because it leads to a SQLITE Error as such \\\"[1] [SQLITE_ERROR] SQL error or missing database (no such column\\\" with the user's downstream pipeline.  \\nInstead, specify all columns in the statement with table namess to make sure the response doesn't return a SQLITE ERROR as such as f\\\"[[SQLITE_ERROR] SQL error or missing database (ambiguous column name:  {column_in_your_response}.\\\" \n\nIn your responses, prioritize accuracy and relevance. If there isn't sufficient information, return `NA`.\n\n"
    #             },
    #             {
    #                 "role": "user",
    #                 "content": user_prompt
    #             }
    #         ],
    #         temperature=0.2,
    #         top_p=0.1,
    #         frequency_penalty=0,
    #         presence_penalty=0,
    #         api_key=configs.openai_api_key
    #     )
    #
    #     gpt_output = chat_completion.choices[0].message.content
    #     # print(gpt_output)
    #
    #     gpt_message_context = [
    #             {
    #                 "role": "system",
    #                 "content": "As a SQL Query Translator GPT, your primary function is to convert the query and its annotations, which are in the form of markdown table and  bullet points, into a sqlite SQL query over a database.\nFirst think step-by-step.\nThen, output the code in a single code block.\nMinimize any other prose.\n\nDO NO INTRODUCE or USE SQL Aliases in your response because it leads to a SQLITE Error as such \\\"[1] [SQLITE_ERROR] SQL error or missing database (no such column\\\" with the user's downstream pipeline.  \\nInstead, specify all columns in the statement with table namess to make sure the response doesn't return a SQLITE ERROR as such as f\\\"[[SQLITE_ERROR] SQL error or missing database (ambiguous column name:  {column_in_your_response}.\\\" \n\nIn your responses, prioritize accuracy and relevance. If there isn't sufficient information, return `NA`.\n\n"
    #             },
    #             {
    #                 "role": "user",
    #                 "content": user_prompt
    #             }
    #         ]
    #     gpt_message_context.append({
    #         "role": "assistant",
    #         "content": gpt_output
    #     })

    #     ran_successfully = False
    #     num_of_trials = 5
    #
    #     # if select not the first word, add schema
    #     while not ran_successfully and num_of_trials > 0:
    #         local_code_interpreter_output, ran_successfully = local_code_interpreter(db_loc,  gpt_output)
    #         if 'SELECT' not in gpt_output[:6]:
    #             table_names = get_db_tables(db_loc)
    #             sch = get_db_schema(db_loc, table_names)
    #             schema_string = represent_schema(sch)
    #             gpt_message_context.append({
    #                 "role": "user",
    #                 "content": f"# Schema: {schema_string} \n# SQL Query:"
    #             })
    #         elif not ran_successfully:
    #             gpt_message_context.append({
    #                 "role": "user",
    #                 "content": local_code_interpreter_output + "\n# SQL Query:"
    #             })
    #             chat_completion = openai.ChatCompletion.create(
    #                 model="gpt-3.5-turbo",
    #                 messages=gpt_message_context,
    #                 temperature=0.2,
    #                 top_p=0.1,
    #                 frequency_penalty=0,
    #                 presence_penalty=0,
    #                 api_key=configs.openai_api_key
    #             )
    #             gpt_output = chat_completion.choices[0].message.content
    #             gpt_message_context.append({
    #                 "role": "assistant",
    #                 "content": gpt_output
    #             })
    #             # print("GPT Debugger")
    #             # print(gpt_output)
    #             num_of_trials -= 1
    # print(gpt_output)
    conn = get_db_conn()

    with conn.cursor() as cur:
        cur.execute(f"""INSERT INTO gatech."spider_dataset_output_sql_query"(db_id, question, gpt_output)
                 VALUES (%s, %s,%s);""", (db_id, inst['question'], gpt_output,))
        conn.commit()
        cur.close()
    conn.close()
    # return gpt_output


if __name__ == '__main__':
    # print(find_column_name("no such column: EMP_LNAME"))
    # print(find_column_name("no such column: Supplier_Addresses.supplier_id"))
    input_table = pd.read_sql("select * from gatech.\"spider_query_understanding\"", get_db_conn())
    output_table = pd.read_sql("select * from gatech.\"spider_dataset_output_sql_query\"", get_db_conn())
    merged_df = pd.merge(input_table, output_table,
                         how='outer', indicator=True,
                         left_on=['db_id', 'question'],
                         right_on=['db_id', 'question']) \
        .query('_merge=="left_only"').drop('_merge', axis=1)

    if len(merged_df) == 0:
        print("Operation Complete.")
    with tqdm.tqdm(total=len(merged_df)) as pbar:
        with ThreadPoolExecutor(max_workers=16) as ex:
            futures = [ex.submit(process_one_record, inst) for idx, inst in merged_df.iterrows()]
            for future in as_completed(futures):
                result = future.result()
                pbar.update(1)
