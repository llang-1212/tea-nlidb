# This code is for v1 of the openai package: pypi.org/project/openai
import re
import os
import time
from concurrent.futures import ThreadPoolExecutor, as_completed

import pandas as pd
import networkx as nx
import openai
import tqdm
from rapidfuzz import process, fuzz

import configs
from join_path_generator import calculate_steiner_tree
from llm_then_steiner import get_db_tables, get_db_entities_samples, get_db_schema
from utils import get_db_conn

completed = False

db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')
df = pd.read_csv('spider_gpt_keyword_operation-annotations.csv')

def represent_schema(schema: dict):
    """

    :param schema: dict with key being table name and value being a list of column names
    :return:
    """
    returned_string = ""
    for k, v in schema.items():
        table_string = f"{k}: " + ', '.join(v) + "\n"
        returned_string += table_string
    return returned_string

def represent_schema_od(schema: dict):
    """

    :param schema: dict with key being table name and value being a list of column names
    :return:
    """
    returned_string = "#\n"
    for k, v in schema.items():
        table_string = f"# {k}(" + ', '.join(v) + ")\n"
        returned_string += table_string
    returned_string += "#\n"
    return returned_string


def represent_table_markdown_samples(table_markdown_dict):
    returned_string = ""
    for k, v in table_markdown_dict.items():
        table_string = f"{k}:\n" + v + "\n"
        returned_string += table_string
    return returned_string


def extract_table_names_from_rough_keyword_annotation(rough_keyword_annotations, db_loc):
    gpt_evidence_set = re.findall(r'`(.*?)`', rough_keyword_annotations.split("Query:")[0])
    new_gpt_evidence_set = []

    for et in gpt_evidence_set:
        parentheses_match = re.match(r"\((.*?)\)", et)
        if parentheses_match is not None:
            new_gpt_evidence_set.append(parentheses_match.string)
        else:
            new_gpt_evidence_set.append(et)
    gpt_evidence_set = new_gpt_evidence_set
    gpt_evidence_set = set([e.split('.')[0] for e in gpt_evidence_set])

    table_names = get_db_tables(db_loc)
    new_gpt_evidence_set = []
    for et in gpt_evidence_set:
        try:
            new_gpt_evidence_set.append(process.extractOne(et, table_names, scorer=fuzz.WRatio)[0])
        except:
            pass
    gpt_evidence_set = set(new_gpt_evidence_set)

    if len(gpt_evidence_set) == 0:
        gpt_evidence_set = table_names

    return gpt_evidence_set


def retrieve_operation_annotations(nl_question, first_keyword_annotations: str, db_loc: str):
    keyword_annotation_evidence_set = extract_table_names_from_rough_keyword_annotation(first_keyword_annotations, db_loc)
    try:
        steiner_tree_graph_nodes = calculate_steiner_tree(db_loc, keyword_annotation_evidence_set).nodes
        possible_additional_tables_to_consider = set([n.split('.')[0] for n in steiner_tree_graph_nodes])
        keyword_annotation_evidence_set = keyword_annotation_evidence_set.union(possible_additional_tables_to_consider)
    except:
        pass
    ds_limit = 10
    ds = get_db_entities_samples(db_loc, keyword_annotation_evidence_set, limit=ds_limit)
    sch = get_db_schema(db_loc, keyword_annotation_evidence_set)
    user_prompt = f"# Query:\n{nl_question}\n\n## Schema:\n{represent_schema(sch)}\n## Each table’s sample of the first {ds_limit} rows:\n{represent_table_markdown_samples(ds)}\n# Annotations:"
    print(user_prompt)

    # completed = False
    # while not completed:
    #     try:
    #         chat_completion = openai.ChatCompletion.create(
    #             model="gpt-3.5-turbo",
    #             messages=[
    #                 {
    #                     "role": "system",
    #                     "content": f"As a Operation Annotator GPT, your primary function is to assign SQL operation types to each word in the user's query when it is needed given the user's query, schema, and each table's sample of the first {ds_limit} rows. Primarily target eight linguistic patterns: cardinality, aggregation, grouping, comparison, assertion, negation, sorting, and distinct. You should respond to queries with a markdown table that has each word in the query in the first column and annotation in the second column. In your response, prioritize accuracy and precision as each word's annotation will be used for a classification task.\n\nNote: Distinguish count from other aggregation functions as it also applies to non-numeric data. \n\n## Examples of SQL methods corresponding to each annotation type\ncardinality: COUNT \naggregation: SUM, MAX, MIN, AVG \ngrouping: GROUP BY \ncomparison: >, <, >=, <= \nassertion: = \nnegation: <>, != \nsorting: ORDER BY, TOP \ndistinct: DISTINCT \n\n## Documentation\n\nGenerating the results of a SELECT statement is presented as a six step process in the description below:\n\n1. FROM clause processing: The input data for the simple SELECT is determined. The input data is either implicitly a single row with 0 columns (if there is no FROM clause) or is determined by the FROM clause.\n\n2. WHERE clause processing: The input data is filtered using the WHERE clause expression.\n\n3. GROUP BY, HAVING and result-column expression processing: The set of result rows is computed by aggregating the data according to any GROUP BY clause and calculating the result-set expressions for the rows of the filtered input dataset.\n\n4. DISTINCT/ALL keyword processing: If the query is a \"SELECT DISTINCT\" query, duplicate rows are removed from the set of result rows.\n\n5. ORDER clause: If the query has ORDER clause, rows are first sorted based on the results of evaluating the left-most expression in the ORDER BY list, then ties are broken by evaluating the second left-most expression and so on.\n\n5. LIMIT clause processing: If the query has LIMIT clause, an upper bound on the number of rows returned by the entire SELECT statement is placed.\n\n"
    #                 },
    #                 {
    #                     "role": "user",
    #                     "content": user_prompt
    #                 }
    #             ],
    #             temperature=0.2,
    #             top_p=0.1,
    #             frequency_penalty=0,
    #             presence_penalty=0,
    #             api_key=configs.openai_api_key
    #         )
    #         completed = True
    #     except openai.error.RateLimitError:
    #         print("get_database_description - openai.error.RateLimitError")
    #         time.sleep(120)
    #     except openai.error.InvalidRequestError:
    #         # openai.error.InvalidRequestError: This model's maximum context length is 4097 tokens. However, your messages resulted in 4216 tokens. Please reduce the length of the messages.
    #         ds_limit = max(ds_limit-1, 0)
    #         if ds_limit == 0:
    #             print("RuntimeError", db_loc, nl_question)
    #             raise RuntimeError
    #         ds = get_db_entities_samples(db_loc, keyword_annotation_evidence_set, limit=ds_limit)
    #         user_prompt = f"# Query:\n{nl_question}\n\n## Schema:\n{represent_schema(sch)}\n## Each table’s sample of the first {ds_limit} rows:\n{represent_table_markdown_samples(ds)}\n# Annotations:"
    #
    # return chat_completion.choices[0].message.content



def each_database_operation(db_id):
    pbar = df[df['db_id'] == db_id].iterrows()
    conn = get_db_conn()
    db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")

    for i, r in pbar:
        # pbar.set_description(f"{db_id} - {i}")
        question = r['question']
        keyword_annotations = r['keyword_annotations']

        # sql_query = r['final_SQL_output']
        sql_generated = retrieve_operation_annotations(question, keyword_annotations, db_loc)

        with conn.cursor() as cur:
            cur.execute(f"""INSERT INTO gatech."spider_dataset_GPT_operation_annotation"(db_id, question, gpt_output)
                     VALUES (%s, %s,%s);""", (db_id, question, sql_generated,))
            conn.commit()
            cur.close()

    conn.close()


def main():
    global df

    persistence_db_table_name = 'gatech."spider_dataset_GPT_operation_annotation"'

    existing_db = pd.read_sql(f"select * from {persistence_db_table_name}", get_db_conn())

    new_df = pd.merge(df, existing_db, how="outer", indicator=True,
                      left_on=['db_id', 'question'], right_on=['db_id', 'question']) \
        .query('_merge=="left_only"').drop('_merge', axis=1)
    df = new_df
    print(new_df)
    unique_db_ids = list(df['db_id'].unique())
    print(df)
    if len(unique_db_ids) == 0:
        print("Operation Complete.")
        return
    with tqdm.tqdm(total=len(unique_db_ids)) as pbar:
        with ThreadPoolExecutor(max_workers=4) as ex:
            futures = [ex.submit(each_database_operation, db_id) for db_id in unique_db_ids]
            for future in as_completed(futures):
                result = future.result()
                pbar.update(1)



if __name__ == '__main__':
    # main()
    test_nl_question = "How many times the number of adults and kids staying in a room reached the maximum capacity of the room?"
    test_nl_question = "What are the first and last names of all students who are living in a dorm with a TV Lounge?"
    # test_nl_question = "What are the names of modern rooms that have a base price lower than $160 and two beds."
    #
    r = df[df['question'] == test_nl_question].iloc[0]
    question = r['question']
    keyword_annotations = r['keyword_annotations']
    operation_annotations = r['operation_annotations']
    use_nested_query = r['use_nested_query']
    db_id = r['db_id']
    db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')

    db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")

    test_response = retrieve_operation_annotations(question, keyword_annotations, db_loc)
    # with open("operation_annotator_test_output.txt", mode="w") as f:
    #     f.write(test_response)
    # print(test_response)

    # from utils import from_md
    #
    # with open("operation_annotator_test_output.txt") as f:
    #     fs = f.read()
    #
    # df = from_md(fs)
    # print(df.columns)
    # print(df.iloc[0]['Annotation'] == '')
    # print(df.iloc[12]['Annotation'])


# sample_str = """
# | Word       | Annotation |
# |------------|------------|
# | Which      |            |
# | buildings  |            |
# | have       |            |
# | more       |            |
# | than       |            |
# | one        |            |
# | company    |            |
# | offices    |            |
# | ?          |            |
# | Give       |            |
# | me         |            |
# | the        |            |
# | building   |            |
# | names      |            |
# | .          |            |
#
# Explanation:
# - "Which" and "buildings" are not relevant to the SQL operations.
# - "have" indicates a cardinality operation.
# - "more than" indicates a comparison operation.
# - "one" indicates a cardinality operation.
# - "company" and "offices" are not relevant to the SQL operations.
# - "?" and "." are not relevant to the SQL operations.
# - "Give" and "me" are not relevant to the SQL operations.
# - "the" is not relevant to the SQL operations.
# - "building" indicates a grouping operation.
# - "names" indicates a result-column expression operation.
# """