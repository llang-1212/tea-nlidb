import sqlite3
from concurrent.futures import ThreadPoolExecutor, as_completed

import pandas as pd
import re
import ast
import os
import json

import tqdm

import configs
from llm_then_steiner import get_db_tables, get_db_schema
from operation_annotator import represent_schema_od
from test_llm_translation_indx import get_db_conn
import openai

# weak_column_labels = pd.read_csv('Spider Dataset Weak Labels V2.csv')
weak_column_labels = pd.read_csv('Spider Dataset Weak Labels NQ.csv')
db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')


def process_one_record(inst):
    db_id = inst['db_id']
    db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")

    nested_query_detected = inst['nqt_len'] > 0
    token_annotations_df = pd.DataFrame(inst['token_annotations'])
    token_annotations_df.rename({'characters': 'token'}, axis=1, inplace=True)
    question = inst['question']
    table_names = get_db_tables(db_loc)
    sch = get_db_schema(db_loc, table_names)

    user_prompt = f"\n### Complete sqlite SQL query only and with explanation\n### SQLite SQL tables, with their properties:\n{represent_schema_od(sch)}### {question}\nSELECT"

    chat_completion = openai.ChatCompletion.create(
        model="gpt-3.5-turbo-0613",
        messages=[
            {
                "role": "user",
                "content": user_prompt
            }
        ],
        temperature=0.2,
        top_p=0.1,
        frequency_penalty=0,
        presence_penalty=0,
        api_key=configs.openai_api_key
    )

    gpt_output = chat_completion.choices[0].message.content

    conn = get_db_conn()

    with conn.cursor() as cur:
        cur.execute(f"""INSERT INTO gatech."spider_openai_default_prompt_baseline_0613"(db_id, question, gpt_output)
                 VALUES (%s, %s,%s);""", (db_id, inst['question'], gpt_output,))
        conn.commit()
        cur.close()
    conn.close()
    # return gpt_output
if __name__ == '__main__':
    input_table = pd.read_sql("select * from gatech.\"spider_query_understanding\"", get_db_conn())
    output_table = pd.read_sql("select * from gatech.\"spider_openai_default_prompt_baseline_0613\"", get_db_conn())
    merged_df = pd.merge(input_table, output_table,
                         how='outer', indicator=True,
                         left_on=['db_id', 'question'],
                         right_on=['db_id', 'question'])\
        .query('_merge=="left_only"').drop('_merge', axis=1)

    if len(merged_df) == 0:
        print("Operation Complete.")
    with tqdm.tqdm(total=len(merged_df)) as pbar:
        with ThreadPoolExecutor(max_workers=16) as ex:
            futures = [ex.submit(process_one_record, inst) for idx, inst in merged_df.iterrows()]
            for future in as_completed(futures):
                result = future.result()
                pbar.update(1)

