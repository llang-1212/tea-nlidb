import json
import os
import re
from concurrent.futures import ThreadPoolExecutor, as_completed

import pandas as pd
import tqdm

from evidence_partitioning import *
from join_path_generator import get_lower_to_correct_dict
from process_sql import get_all_database_elements
from utils import from_md, get_db_conn, is_close_subsequence, finding_substring_idx
from athena_pp.nested_query_detection import AnnotatedToken, tokenize, convert_string_idx_to_token_idx, \
    get_database_schema, remove_punctuation
import spacy
from numerizer import numerize


db_dir = os.getcwd()[:9] + os.path.join('tonyk', 'Downloads', 'spider', 'spider', 'database')
df = pd.read_csv('Spider Train Set Nested Query Dection.csv')


def extract_between_backticks(text):
    # Find all strings between backticks
    matches = re.findall(r'`(.*?)`', text)
    return matches[0]


def extract_between_parentheses(text):
    # Find all strings between parentheses
    matches = re.findall(r'\((.*?)\)', text)
    if len(matches) == 0:
        return text
    else:
        return matches[0]


def extract_table_name(database_element_string):
    database_element_string_split = database_element_string.split(".")
    table_name, column_name = None, None
    if len(database_element_string_split) == 2:
        table_name, column_name = database_element_string_split
    elif len(database_element_string_split) == 1:
        table_name, = database_element_string_split
    return table_name, column_name


def extract_equal_with_variables_on_each_side(checked_string):
    # return re.match("[a-zA-Z0-9._\-'\"]+\s*=\s*[[a-zA-Z0-9._\-'\"]+$", checked_string)
    return re.match("^\s*\w+(\.\w+)?\s*=\s*(?:'[^']*'|\"[^\"]*\"|\w+|\d+)\s*$", checked_string)


def extract_instance_token(string_w_equality):
    # return re.match("[a-zA-Z0-9._\-'\"]+\s*=\s*[[a-zA-Z0-9._\-'\"]+$", checked_string)
    # return re.search('\="([^"]*)"|\'([^"]*)\'|\d+', string_w_equality)
    # return re.search('"([^"]*)"|\'([^"]*)\'|\d+', string_w_equality)
    # return re.search('"([^"]*)"|\'([^"]*)\'', string_w_equality)
    found_tokens = re.findall('\'([^\']*)\'|"([^"]*)"', string_w_equality)
    found_tokens = ["".join(x) for x in found_tokens]
    if len(found_tokens) > 0:
        return found_tokens
    else:
        return None

# def extract_comparison_with_variables_on_each_side(checked_string):
#     return re.match("[a-zA-Z0-9._\-'\"]+\s*([\>\<]|\>\=|\<\=)\s*[[a-zA-Z0-9._\-'\"]+$", checked_string)

def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False


def map_annotation_to_query_token_list(annotation_df, token_annotations, annotation_map_func=lambda x: x):
    t = 0
    i = 0
    j = 0
    while i < len(token_annotations) and j < len(annotation_df):
        ta_chars = token_annotations[i].characters
        oa_chars = annotation_df.iloc[j, 0]
        # print(ta_chars, oa_chars)
        if ta_chars.lower() in oa_chars.lower():
            token_annotations[i].set_operation_annotation(annotation_map_func(annotation_df.iloc[j, 1]), None)
            i += 1
            j += 1

        else:
            j += 1
        t += 1
        if t > (len(token_annotations) + len(annotation_df)) * 2:
            break
    return token_annotations


def operation_annotation_v_112823_label_map(db_loc):
    db_elements = get_all_database_elements(db_loc)
    db_elements = [de.lower() for de in db_elements]

    def labeling_func(label):
        if label is None:
            return None

        if label.lower() in ['cardinality', 'aggregation', 'grouping', 'comparison', 'negation', 'sorting', 'distinct']:
            return label.lower()
        if label.lower() in ['assertion', 'equality']:
            return 'equality'
        if label.lower() in ['<', '>', '>=', '<=']:
            return 'comparison'
        else:
            if label.lower() in db_elements:
                return 'comparison'

    return labeling_func


def transform_list_of_instances_to_pandas_dataframe(list_of_instances):
    return pd.DataFrame(ta.__dict__ for ta in list_of_instances)

def nested_query_annotator(Q, gpt_oa_df, gpt_es_df, gpt_vp_df, db_loc, database_tables_info):
    Q = remove_punctuation(Q)
    # global tokens, is_measure, jt
    token_annotations = [AnnotatedToken(i, t) for i, t in enumerate(tokenize(Q))]
    # print(gpt_es_df)
    # print(gpt_oa_df)
    # print(gpt_vp_df)
    # print([ta.characters for ta in token_annotations])
    # postprocess evidence set
    evidence_set_annotations = []
    db_elements = get_all_database_elements(db_loc)
    db_elements = [de.lower() for de in db_elements]

    # specify whether the token is a numeric value
    for ta in token_annotations:
        ta.set_numeric_annotation(isfloat(numerize(ta.characters)))
    # map evidence annotation to token
    print("mapping evidence annotation to token...")
    print("evidence annotation:")
    print(gpt_es_df)
    for _, l in gpt_es_df.iterrows():
        try:
            selected_query_phrase = l.iloc[0]
            selected_query_phrase_database_element = l.iloc[1]
            try:
                selected_query_phrase_database_element = extract_between_backticks(selected_query_phrase_database_element)
                selected_query_phrase_database_element = extract_between_parentheses(selected_query_phrase_database_element)
                table_name, column_name = extract_table_name(selected_query_phrase_database_element)

            except TypeError:
                continue

            matches_to_database_element = selected_query_phrase_database_element.lower() in db_elements or \
                                          (selected_query_phrase_database_element.split(".")[1] == '*' and
                                           selected_query_phrase_database_element.split(".")[0].lower() in db_elements)

            if matches_to_database_element:
                evidence_set_annotations.append({
                    'tokens': selected_query_phrase,
                    'table_name': table_name,
                    'column_name': column_name
                })
        except IndexError:
            pass
    for es in evidence_set_annotations:
        tokens, table_name, column_name = es['tokens'], es['table_name'], es['column_name']

        is_measure = False
        if table_name is not None and table_name.lower() in db_elements:
            if column_name is not None and column_name.lower() in db_elements:
        # print(database_tables_info)
                column_datatype = database_tables_info[table_name.lower()][column_name.lower()]
                is_measure = column_datatype == 'number' and 'id' not in column_name.lower()
        try:
            start_idx, end_idx = is_close_subsequence(Q, tokens)
        except IndexError: # not found
            continue
        for token_annot in token_annotations[start_idx:end_idx]:
            token_annot.set_associated_database_element(table_name, column_name)
            token_annot.set_measure_annotation(is_measure)
    # annotating dependency
    print("annotating dependency...")

    nlp = spacy.load("en_core_web_trf")
    spacy_parsed = nlp(Q)
    t = 0
    i = 0
    j = 0
    while i < len(token_annotations) and j < len(spacy_parsed):
        ta_chars = token_annotations[i].characters
        spacy_token = spacy_parsed[j]

        if ta_chars.lower() in spacy_token.text.lower():
            # print(ta_chars, spacy_token.like_num)

            token_annotations[i].set_numeric_annotation(spacy_token.like_num)
            token_annotations[i].set_dependent_idxs(
                [convert_string_idx_to_token_idx(Q, child.idx) for child in spacy_token.children] +
                [convert_string_idx_to_token_idx(Q, spacy_token.head.idx)]
            )

            # token_annotations[i].set_focus(spacy_token.pos)

            # token_annotations[i].set_operation_annotation(operation_annotation_df.iloc[j, 1], None)
            i += 1
            j += 1

        else:
            j += 1
        t += 1
        if t > (len(token_annotations) + len(spacy_parsed)) * 2:
            break
    for st in spacy_parsed:
        # print(st, st.dep_)
        if st.dep_ == 'ROOT':
            for c in st.children:
                try:
                    token_annotations[len(tokenize(Q[:c.idx]))].set_focus(True)
                except IndexError:
                    pass
                    # print(c.idx, c)
    # for ta in token_annotations:
    #     print(ta.characters, ta.focus)
    # print()
    # annotating operation
    print("mapping operation annotation to token...")
    print("operation annotation:")
    print(gpt_oa_df)
    token_annotations = map_annotation_to_query_token_list(gpt_oa_df, token_annotations,
                                                           operation_annotation_v_112823_label_map(db_loc))
    # annotating value predictor
    postprocessed_value_predictor = []
    postprocessed_value_predictor_instance_label = []
    print("mapping value prediction to token...")
    print("value prediction:")
    print(gpt_vp_df)
    for _, l in gpt_vp_df.iterrows():
        try:
            # print(l.iloc[0], extract_equal_with_variables_on_each_side(l.iloc[2]), l.iloc[2])
            extract_equal_with_variables_on_each_side(l.iloc[2]), l.iloc[2]
        except TypeError:
            continue
        checking_for_instance_variable = extract_instance_token(l.iloc[2])
        if checking_for_instance_variable is not None:
            for matched_string in checking_for_instance_variable:
            # matched_string = checking_for_instance_variable.string[
            #                  checking_for_instance_variable.start():
            #                  checking_for_instance_variable.end()
            #                  ]
            #     print(checking_for_instance_variable)
            #     print(matched_string)

                if isinstance(matched_string, tuple):
                    matched_string = matched_string[0]
                matched_string = matched_string.strip('\'')
                matched_string = matched_string.strip('"')
                # print("cfv", Q, "|", matched_string)
                try:
                    substring_idx = Q.index(matched_string)
                except ValueError:
                    continue
                start_idx = len(tokenize(Q[:substring_idx]))
                end_idx = len(tokenize(Q[:substring_idx + len(matched_string)]))
                # print(matched_string, substring_idx, start_idx, end_idx)
                for i in range(start_idx, end_idx):
                    token_annotations[i].set_instance_type(True)
        if extract_equal_with_variables_on_each_side(l.iloc[2]) is not None:

            if l[1] is not None and l.iloc[1].lower() == 'comparison' and len(l.iloc[0]) > 0:
                l.iloc[1] = 'equality'
                postprocessed_value_predictor.append(l)
    # print(postprocessed_value_predictor)
    postprocessed_value_predictor_df = pd.DataFrame(postprocessed_value_predictor)
    for _, annot in postprocessed_value_predictor_df.iterrows():
        tokens = annot.iloc[0]
        # tokens, operation_type, annotation = annot['tokens'], annot['operation_type'], annot['annotation']
        # if len(tokens) == 0:
        #     continue
        # print(tokens, operation_type, annotation)
        start_idx, end_idx = finding_substring_idx(tokens, Q)
        # print(start_idx, end_idx, tokens)

        start_idx = len(Q[:start_idx].split())
        end_idx = len(Q[:end_idx + 1].split())
        # print(start_idx, end_idx, tokens)
        # print(start_idx, end_idx, annot, annot.iloc[2])
        for token_annot in token_annotations[start_idx:end_idx]:
            token_annot.set_operation_annotation(annot.iloc[1], annot.iloc[2])
    # for ta in token_annotations:
    #     print(ta.characters, " | ", ta.operation_annotation, ta.raw_operation_annotation, " | ",
    #           ta.related_database_table, ta.related_database_column, ' | ', ta.is_instance_type, ta.focus, ta.numeric)

    print()
    print("Input to nested query annotator...")
    print(transform_list_of_instances_to_pandas_dataframe(token_annotations))

    nqTokens = []
    aggTokens = []
    joinTokens = []
    for t_annot in token_annotations:
        if t_annot.operation_annotation == 'cardinality' or t_annot.operation_annotation == 'aggregation':
            aggTokens.append(t_annot)
        elif t_annot.operation_annotation == 'comparison' or t_annot.operation_annotation == 'negation':
            joinTokens.append(t_annot)
    print("aggregation tokens:")
    print(transform_list_of_instances_to_pandas_dataframe(aggTokens))
    print("join tokens:")
    print(transform_list_of_instances_to_pandas_dataframe(joinTokens))
    if not aggTokens and joinTokens:
        for jt in joinTokens:
            deps = jt.dependents_idxs
            dts_entity = [token_annotations[d] for d in deps if
                          d < len(token_annotations) and token_annotations[d].related_database_table is not None]

            try:
                jt_prev = token_annotations[jt.word_idx - 1]
            except IndexError:
                jt_prev = None
            try:
                jt_next = token_annotations[jt.word_idx + 1]
            except IndexError:
                jt_next = None

            # print(dts_entity)
            # print(jt_prev)
            # print(jt_next)

            if jt.operation_annotation == 'negation' or jt.operation_annotation == 'equality':
                if jt_next is not None and jt_next in dts_entity and \
                        jt_prev is not None and jt_prev in dts_entity and \
                        jt_next.related_database_table == jt_prev.related_database_table:
                    jt.nested_query_type = 'TYPE-N'
                elif (jt_next is not None and jt_next in dts_entity) or (jt_prev is not None and jt_prev in dts_entity):
                    jt.nested_query_type = 'TYPE-J'

            if jt.operation_annotation == 'comparison' and \
                    jt_prev is not None and jt_prev.is_measure and \
                    jt_next is not None and jt_next.numeric:
                jt.nested_query_type = 'TYPE-N'
            nqTokens.append(jt)

    elif aggTokens and not joinTokens:
        for at in aggTokens:
            if at.operation_annotation == 'aggregation':
                deps = at.dependents_idxs
                for d in deps:
                    if d >= len(token_annotations):
                        continue
                    dt = token_annotations[d]
                    if dt.related_database_table is not None:
                        if at.word_idx > dt.word_idx:
                            at.nested_query_type = 'TYPE-A'
                            nqTokens.append(at)
                            break

    elif aggTokens and joinTokens:
        for jt in joinTokens:
            deps = jt.dependents_idxs
            dts_measure = [token_annotations[d] for d in deps if
                           (d < len(token_annotations) and token_annotations[d].is_measure)]
            try:
                jt_prev = token_annotations[jt.word_idx - 1]
            except IndexError:
                jt_prev = None
            if jt.operation_annotation == 'comparison' and jt_prev in dts_measure:
                jt.nested_query_type = 'TYPE-JA'
                nqTokens.append(jt)

        for at in aggTokens:
            deps = at.dependents_idxs
            dts_entity = [token_annotations[d] for d in deps if
                          (d < len(token_annotations) and token_annotations[d].related_database_table is not None)]
            dts_comparison = [token_annotations[d] for d in deps if (
                    d < len(token_annotations) and token_annotations[d].operation_annotation == 'comparison')]
            if at.operation_annotation == 'aggregation' and True and len(dts_entity) > 0 and len(dts_comparison) > 0:
                at.nested_query_type = 'TYPE-JA'
                nqTokens.append(at)

    token_annotations_df = pd.DataFrame(ta.__dict__ for ta in token_annotations)
    nqTokens_df = pd.DataFrame(ta.__dict__ for ta in nqTokens)

    print("Nested Query Tokens Classsified:")
    print(nqTokens_df)
        # Query partitioning
    for nqt in nqTokens:
        try:
            nqt_idx = nqt.word_idx
            evidence_set_before = token_annotations[:nqt_idx]
            evidence_set_after = token_annotations[nqt_idx:]

            # print("NQT:", nqt.characters, nqt.nested_query_type)
            # print(evidence_set_before)
            # print(evidence_set_after)
            if nqt.nested_query_type == 'TYPE-N':
                evidence_set_before, evidence_set_after = apply_heuristics(evidence_set_before, nqt, evidence_set_after,
                                                                           heuristics_instance_sharing,
                                                                           heuristics_focus_sharing
                                                                           )
            elif nqt.nested_query_type == 'TYPE-A':
                # print("applyHeuristic(ES1, ES2, H5, H6)")
                evidence_set_before, evidence_set_after = apply_heuristics(evidence_set_before, nqt, evidence_set_after,
                                                                           heuristics_instance_sharing_in_numeric_comparison,
                                                                           )
            elif nqt.nested_query_type == 'TYPE-J':
                evidence_set_before, evidence_set_after = apply_heuristics(evidence_set_before, nqt, evidence_set_after,
                                                                           heuristics_co_reference,
                                                                           heuristics_instance_sharing,
                                                                           heuristics_focus_sharing,
                                                                           heuristics_instance_sharing_in_numeric_comparison

                                                                           )
            elif nqt.nested_query_type == 'TYPE-JA':
                evidence_set_before, evidence_set_after = apply_heuristics(evidence_set_before, nqt, evidence_set_after,
                                                                           heuristics_co_reference,
                                                                           heuristics_instance_sharing,
                                                                           heuristics_instance_sharing_in_numeric_comparison
                                                                           )
            outer_query = sorted(evidence_set_before, key=lambda e: e.word_idx)
            inner_query = sorted(evidence_set_after, key=lambda e: e.word_idx)
            print("Nested Query Token", "\"", nqt.characters, "\"", "'s division of query:")
            print(transform_list_of_instances_to_pandas_dataframe(outer_query))

            print("outer^---------------inner")
            print(transform_list_of_instances_to_pandas_dataframe(inner_query))


            # join condition generation
            nqTokens_df[['outer_query', 'inner_query']] = [
                json.dumps(transform_list_of_instances_to_pandas_dataframe(outer_query).to_json()),
                json.dumps(transform_list_of_instances_to_pandas_dataframe(inner_query).to_json())]

            join_condition_before, join_condition_after = [], []
            for d in set(nqt.dependents_idxs):
                if d < nqt.word_idx and token_annotations[d].related_database_table is not None:
                    join_condition_before.append(token_annotations[d])
                elif d > nqt.word_idx and token_annotations[d].related_database_table is not None:
                    join_condition_after.append(token_annotations[d])
            print("join conditions-------")
            print("from outer", [f"[{jcb.word_idx}] {jcb.characters}  - {jcb.related_database_table}.{jcb.related_database_column if jcb.related_database_column is not None else '*'}" for jcb in
                   join_condition_before])
            print("---------")
            print("from inner", [f"[{jcb.word_idx}] {jcb.characters}  - {jcb.related_database_table}.{jcb.related_database_column if jcb.related_database_column is not None else '*'}" for jcb in
                   join_condition_after])

            # print(nqt.word_idx, nqt.dependents_idxs)

            nqTokens_df[['join_condition_before', 'join_condition_after']] = [
                transform_list_of_instances_to_pandas_dataframe(join_condition_before).to_json(),
                transform_list_of_instances_to_pandas_dataframe(join_condition_after).to_json()]
        except IndexError:
            continue

    return token_annotations_df, nqTokens_df



def each_database_operation(db_id):
    pbar = df[df['Column1.db_id'] == db_id].iterrows()
    conn = get_db_conn()
    db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")
    database_tables_info = get_database_schema(db_id, return_table_dict_w_table_name=True)
    for i, r in pbar:
        # pbar.set_description(f"{db_id} - {i}")
        question = r['question']
        gpt_es = r['gpt_evidence_set']
        gpt_oa = r['gpt_operation_annotation']
        gpt_vp = r['gpt_value_prediction']


        gpt_es_df = from_md(gpt_es.split('Query:')[0])
        # try:
        gpt_oa_df = from_md(gpt_oa.split('Query:')[0])
        # except ValueError:
        #     print("gpt_oa", db_id, question)
        #     print(gpt_oa)
        #     raise ValueError
        gpt_vp_df = from_md(gpt_vp.split('Query:')[0])
        # print(gpt_oa_df)
        # print(gpt_es_df)
        # print(gpt_vp_df)
        token_annotations_df, nqTokens_df = nested_query_annotator(question, gpt_oa_df, gpt_es_df, gpt_vp_df, db_loc, database_tables_info)

        # sql_query = r['final_SQL_output']
        # sql_generated = retrieve_operation_annotations(question, keyword_annotations, db_loc)
        #
        with conn.cursor() as cur:
            cur.execute(f"""INSERT INTO gatech."spider_query_understanding"(db_id, question, 
            token_annotations,
            nested_query_annotations,
            nqt_len)
                     VALUES (%s, %s, %s, %s, %s);""", (db_id, question,
                                              token_annotations_df.to_json(),
                                              nqTokens_df.to_json(),
                                              len(nqTokens_df),))
            conn.commit()
            cur.close()

    conn.close()


def main():
    global df

    persistence_db_table_name = 'gatech."spider_query_understanding"'

    existing_db = pd.read_sql(f"select * from {persistence_db_table_name}", get_db_conn())
    print(df.columns, existing_db.columns)
    new_df = pd.merge(df, existing_db, how="outer", indicator=True,
                      left_on=['db_id', 'question'], right_on=['db_id', 'question']) \
        .query('_merge=="left_only"').drop('_merge', axis=1)
    df = new_df
    print(new_df)
    unique_db_ids = list(df['db_id'].unique())
    print(df)
    if len(unique_db_ids) == 0:
        print("Operation Complete.")
        return
    with tqdm.tqdm(total=len(unique_db_ids)) as pbar:
        with ThreadPoolExecutor(max_workers=4) as ex:
            futures = [ex.submit(each_database_operation, db_id) for db_id in unique_db_ids]
            for future in as_completed(futures):
                result = future.result()
                pbar.update(1)




if __name__ == '__main__':
    Q = "Show names for all aircrafts with distances more than the average."
    # # Q = "What are the names of all aircrafts that can cover more distances than average?"
    # # Q = "How many courses does the department of Computer Information Systems offer?"
    # # Q = "How many sections does course ACCT-211 has?"
    # # Q = "How many albums are there?"
    # # Q = "What are all company names that have a corresponding movie directed in the year 1999?"
    # # Q = "Find the distinct details of invoices which are created before 1989-09-03 or after 2007-12-25."
    # # Q = "Show the name and number of employees for the departments managed by heads whose temporary acting value is \\'Yes\'?"
    # Q = "Show names and seatings, ordered by seating for all tracks opened after 2000."
    # # print(extract_equal_with_variables_on_each_side("STAY.ROOM = 111"))
    # print(extract_instance_token('STAY.ROOM = 111'))
    # print(extract_instance_token("INVOICES.invoice_date > '2007-12-25'"))
    #
    gpt_es = pd.read_sql(f"select * from gatech.\"spider_dataset_GPT_labels\" where question = '{Q}'", get_db_conn())
    gpt_oa = pd.read_sql(f"select * from gatech.\"spider_dataset_GPT_operation_annotation\" where question = '{Q}'",
                         get_db_conn())
    gpt_vp = pd.read_sql(f"select * from gatech.\"spider_dataset_GPT_operation_annotation_1\" where question = '{Q}'",
                         get_db_conn())
    if len(gpt_es) != 1 or len(gpt_oa) != 1 or len(gpt_vp) != 1:
        print(len(gpt_es), len(gpt_oa), len(gpt_vp))
        raise RuntimeError
    db_id = gpt_oa.db_id.iloc[0]
    db_loc = os.path.join(db_dir, db_id, db_id + ".sqlite")
    print(gpt_es.columns)
    print(gpt_oa.columns)
    gpt_oa_df = from_md(gpt_oa.gpt_output.iloc[0].split('Query:')[0])
    gpt_es_df = from_md(gpt_es.gpt_output.iloc[0].split('Query:')[0])
    gpt_vp_df = from_md(gpt_vp.gpt_output.iloc[0].split('Query:')[0])

    database_tables_info = get_database_schema(db_id, return_table_dict_w_table_name=True)

    # raise EOFError
    # Q = "How many credits does course CIS-220 have, and what its description?"
    nested_query_annotator(Q, gpt_oa_df, gpt_es_df, gpt_vp_df, db_loc, database_tables_info)



    # main()
# Write a query for the following annotation. All of the database elements are present.
#
# natural language query | database operation | database element
# --------------------------------------------
# average | aggregation None  |  aircraft.distance


# The inner query of a nested query is supported in FROM, WHERE,
# and HAVING clauses of the outer query.